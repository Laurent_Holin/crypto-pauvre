\babel@toc {french}{}
\beamer@sectionintoc {1}{Motivation et cadre g\'en\'eral}{3}{0}{1}
\beamer@sectionintoc {2}{Fondements math\'ematiques}{4}{0}{2}
\beamer@subsectionintoc {2}{1}{Variations sur la notion d'associativit\'e}{5}{0}{2}
\beamer@subsectionintoc {2}{2}{Alg\`ebre de Cayley}{6}{0}{2}
\beamer@subsectionintoc {2}{3}{Doublement de Cayley-Dickson}{7}{0}{2}
\beamer@subsectionintoc {2}{4}{Entiers de Cayley}{9}{0}{2}
\beamer@sectionintoc {3}{Informatique}{13}{0}{3}
\beamer@subsectionintoc {3}{1}{RSA octonionique}{14}{0}{3}
\beamer@subsectionintoc {3}{2}{ODC}{20}{0}{3}
\beamer@sectionintoc {4}{Conclusion}{23}{0}{4}
