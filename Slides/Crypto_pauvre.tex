\documentclass[a4paper, 10pt, french]{beamer}

\usepackage[latin1]{inputenc}

\usepackage[T1]{fontenc}

\usepackage{babel}

\usepackage[babel=true]{csquotes}

\usepackage{color}

\usepackage{keyval}

\usepackage{listings}

\usepackage{ragged2e}

\usepackage{hhline}

\usepackage{stmaryrd}          % for yet more math symbols


\setbeamertemplate{frametitle}[default][center]
\setbeamertemplate{footline}[frame number]
\setbeamertemplate{caption}{\raggedright\insertcaption\par}

\setbeamerfont{caption}{size=\scriptsize}

\apptocmd{\frame}{}{\justifying}{}


\DeclareMathOperator{\cardinal}{card}
\DeclareMathOperator{\Module}{Mod}

\setlength{\abovecaptionskip}{-1ex}
\setlength{\belowcaptionskip}{-1ex}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{frame=tb,
  language=Python,
  extendedchars=true
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3
}


\begin{document}

\title{Cryptographie sur des \\ structures alg�briques pauvres}

\author{Laurent Holin}

\institute{MP-Informatique \linebreak Positionnements th�matiques:
alg�bre et informatique th�orique.}

\date{}

\frame{\titlepage}


\begin{frame}{Plan}

\frametitle{Plan}
\tableofcontents

\end{frame}


\section[Motivation]{Motivation et cadre g�n�ral}

\begin{frame}

\frametitle{Motivation et cadre g�n�ral}

Les structures alg�briques effectivement utilis�es par les m�thodes cryptographiques actuelles sont riches,
c'est-�-dire dot�es de propri�t�s fortes, a minima l'associativit�. Les structures plus pauvres propos�es jusqu'� pr�sent, les tresses notamment, n'ont pas r�ussi � s'imposer. Le choix de ce travail est d'explorer n�anmoins
la piste de l'utilisation de structures alg�briques pauvres (celles utilis�es ici ne seront ni commutatives ni m�me
associatives).
Une des id�es sous-jacentes est que la pauvret� peut interdire certaines attaques permises par les structures plus riches; ce point ne sera n�anmoins pas d�velopp� ici.

Le c{\oe}ur de ce travail est impl�mentation d'algorithmes (publi�s) construits � l'aide d'objets
\enquote{exotiques}, et la comparaison, tant qu'� faire se peut, avec des algorithmes standards (RSA et AES), notamment en mati�re de complexit� algorithmique.

Les objets utilis�s par ce travail sont d�finis � l'aide du processus de doublement de Cayley-Dickson, qui est celui permettant de d�finir les complexes � partir des r�els, les quaternions � partir des complexes et les octonions � partir des quaternions. Une diff�rence notable par rapport au cas r�el est l'utilisation d'une
\enquote{constante de structure} � chaque doublement (chaque doublement peut avoir une constante diff�rente).

\end{frame}


\section[Math]{Fondements math�matiques}

\begin{frame}

\begin{center}
{\LARGE
Fondements math�matiques
}
\end{center}

\end{frame}


\subsection[Associativit�]{Variations sur la notion d'associativit�}

\begin{frame}

\frametitle{Variations sur la notion d'associativit�}

Nous allons avoir besoin de notions plus faibles que l'associativit�. Rappelons tout d'abord que l'on appelle magma la donn�e d'un couple $(S,f)$ o� $S$ est un ensemble et $f$ une loi sur $S$. Un sous-magma de$(S,f)$ est un couple $(T,g)$ o� $T \subset S$ et $g$ est la restriction de $f$ � $T$.
Soit $(S,f)$ un magma, nous dirons que $f$ est di-associative si et seulement si chaque sous-magma engendr� par deux �l�ments non n�cessairement distincts est associatif, et que $f$ est alternative si et seulement si:
$(\forall\thinspace x \in S)(\forall\thinspace y \in S) (\forall\thinspace z \in S)$
$[(x = y \text{   ou }   x = z \text{   ou }   y = z) \Rightarrow f(f(x,y),z) = f(x,f(y,z)]$.

\end{frame}


\subsection[Alg�bre de Cayley]{Alg�bre de Cayley}

\begin{frame}

\frametitle{Alg�bre de Cayley}

\begin{center}
\begin{figure}
\includegraphics[scale=0.4]{Arthur_Cayley}
\caption{Arthur Cayley}
\end{figure}
\end{center}

Soit $A$ un anneau, et $(E,+,\times,.)$ une alg�bre sur $A$ (ceci est une g�n�ralisation de la notion d'alg�bre vue en cours, et ici $\times$ n'est pas n�cessairement associatif); une conjugaison sur $E$ est une fonction
$\sigma: E \rightarrow E$ qui v�rifie:
\vskip-0.25em
{
\footnotesize
\begin{enumerate}
\item \(\sigma(e) = e\).
\item \((\forall\thinspace (x,y) \in E^{2})\thickspace
\sigma(x \times y) = \sigma(y) \times \sigma(x)\) (\emph{attention}: interversion de \(x\) et \(y\)!).
\item \((\forall\thinspace x \in E)\thickspace (x+\sigma(x)) \in A \cdot e\).
\item \((\forall\thinspace x \in E)\thickspace (x \times \sigma(x)) \in A \cdot e\).
\end{enumerate}
}

Une alg�bre de Cayley sur $A$ est une structure alg�brique $(E,+,\times,.,\sigma)$ o�
$(E,+,\times,.)$ est une alg�bre sur $A$
et $\sigma$ est une conjugaison sur $E$. Sur une alg�bre de Cayley nous d�finirons la norme de Cayley par ${\cal{N}}(x) = x \times \sigma(x)$ et la trace de Cayley par
${\cal{T}}(x) = x + \sigma(x)$.

\end{frame}


\subsection[Doublement]{Doublement de Cayley-Dickson}

\begin{frame}

\frametitle{Doublement de Cayley-Dickson: d�finition}

\begin{figure}
\includegraphics[scale=0.2]{Dickson}
\caption{Leonard Eugene Dickson}
\end{figure}

Soit $A$ un anneau, et $(E,+,\times,.,\sigma)$ une alg�bre de Cayley sur $A$, dont l'�l�ment neutre pour $+$
sera not� $0$ et l'�l�ment neutre pour $\times$ sera not� $e$.
Notons $F = E \times E$ et $\epsilon = (e,0)$ et choisissons un �l�ment $\zeta \in A$. Nous pouvons munir $F$
d'une structure d'alg�bre de Cayley sur $A$ en d�finissant les fonctions suivantes:

\begin{center}
\(
\footnotesize
\begin{aligned}
\dagger\negthinspace : & &  F \times F & & \rightarrow & & & F \\
 & & ((x,y),(x',y')) & &\mapsto & & & (x+x',y+y') \\
 *\negthinspace : & &  F \times F & & \rightarrow & & & F \\
  & & ((x,y),(x',y')) & & \mapsto & & & (x\times x' - \zeta \cdot (\sigma(y')\times y),y \times \sigma(x') + y'\times x) \\
  \bullet\negthinspace : & &  A \times F & & \rightarrow & & & F \\
  & & (\lambda,(x,y)) & & \mapsto & & & (\lambda \cdot x, \lambda \cdot y) \\
  s\negthinspace : & &  F & & \rightarrow & & & F \\
  & & (x,y) & & \mapsto & & & (\sigma(x),-y)
\end{aligned}
\)
\end{center}

\end{frame}


\begin{frame}

\frametitle{Doublement de Cayley-Dickson: propri�t�s}

L'�l�ment neutre pour $*$ est $\epsilon$, $*$ est commutative si et seulement si $\times$ est commutative et
$\sigma$ est l'identit� sur $E$, $*$ est associative si et seulement si $\times$ est commutative et associative,
$*$ est alternative si et seulement si $\times$ est associative.

Il est possible de d�montrer que lorsque $(F,*)$ est alternatif, il est de surcroit di-associatif.

\end{frame}


\subsection[Propri�t�s]{Entiers de Cayley}

\begin{frame}

\frametitle{Entiers de Cayley: d�finition}

\vskip-1em

\begin{columns}[onlytextwidth]
\begin{column}{.45\textwidth}
\begin{figure}
  \includegraphics[scale=0.155]{William_Rowan_Hamilton_painting}
  \caption{William Rowan Hamilton}
\end{figure}
\end{column}
\hfill
\begin{column}{.45\textwidth}
\begin{figure}
  \includegraphics[scale=1]{Graves_John}
  \caption{John Thomas Graves}
\end{figure}
\end{column}
\end{columns}

\vskip1em

Les objets que nous consid�rons pour ce travail, appel�s entiers de Cayley, sont construits � partir d'un anneau de congruence ${\mathbb{Z}}_{n} = \mathbb{Z} /\! n \mathbb{Z}$ o� $n$ n'est pas n�cessairement premier.
Nous consid�rons d'abord ${\mathbb{Z}}_{n}$ comme alg�bre sur lui-m�me (jouant un r�le analogue aux r�els),
puis choisissant $\alpha \in {\mathbb{Z}}_{n}$ nous construisons ${\mathbb{Z}}_{n;\alpha}$
(jouant un r�le analogue aux complexes) le doublement de Cayley-Dickson de ${\mathbb{Z}}_{n}$ utilisant
$\alpha$, choisissant $\beta \in {\mathbb{Z}}_{n}$ nous construisons ${\mathbb{Z}}_{n;\alpha,\beta}$
(jouant un r�le analogue aux quaternions) le doublement de Cayley-Dickson de ${\mathbb{Z}}_{n;\alpha}$
utilisant $\beta$, et enfin choisissant $\gamma \in {\mathbb{Z}}_{n}$ nous construisons
${\mathbb{Z}}_{n;\alpha,\beta,\gamma}$ (jouant un r�le analogue aux octonions)
le doublement de Cayley-Dickson de ${\mathbb{Z}}_{n;\alpha,\beta}$ utilisant $\gamma$.

\end{frame}


\begin{frame}

\frametitle{Entiers de Cayley: propri�t�s}

Le nombre d'�l�ments inversibles des entiers de Cayley, lorsque $p$ est premier, $s \in {\mathbb{N}}^{*}$,
est donn� dans le tableau suivant:

\begin{center}
{\renewcommand{\arraystretch}{1.5}
\footnotesize
\begin{table}
\begin{tabular}{||l||c|c||}
\hhline{~|t:==:t|}
\multicolumn{1}{c||}{(\(\alpha\beta\gamma \in \mathbb{Z}_{p^{s}}^{*}\))} & \(p \neq 2\) & \(p = 2\) \\[1mm]
\hhline{|t:=::=|=||}
\(\cardinal(\mathbb{Z}_{p^{s}}^{*})\) & \(p^{s-1}(p-1)\) & \(2^{s-1}\) \\[1mm]
\hhline{||---||}
\(\cardinal(\mathbb{Z}_{p^{s};\alpha}^{*})\) & \(p^{2s}-p^{2(s-1)}\left[p+\left(\frac{\alpha_{0}}{p}\right)\thinspace(p-1)\thinspace(-1)^{\frac{p-1}{2}}\right]\) & \(2^{2s-1}\) \\[1mm]
\hhline{||---||}
\(\cardinal(\mathbb{Z}_{p^{s};\alpha,\beta}^{*})\) & \(p^{4s} - p^{4(s-1)}\left[p^{3}+(p-1)\thinspace p\right]\) & \(2^{4s-1}\) \\[1mm]
\hhline{||---||}
\(\cardinal(\mathbb{Z}_{p^{s};\alpha,\beta,\gamma}^{*})\) & \(p^{8s} - p^{8(s-1)}\left[p^{7}+(p-1)\thinspace p^{3}\right]\) & \(2^{8s-1}\) \\[1mm]
\hhline{|b:=:b:==:b|}
\end{tabular}
\end{table}
}
\end{center}

La premi�re ligne n'est bien entendu que la redite de la valeur de l'indicatrice d'Euler.

\end{frame}


\begin{frame}

\frametitle{Entiers de Cayley: le \enquote{th�or�me chinois}}

Le \enquote{th�or�me chinois} se g�n�ralise aux entiers de Cayley. Ainsi, soit \allowbreak $n = \prod_{i=1}^{k}{n}_{i}$ o�
les ${n}_{i}\in {\mathbb{N}}^{*}$ sont deux � deux premiers entre eux, alors ${\mathbb{Z}}_{n}$ et
${\mathbb{Z}}_{{n}_{1}}\vartimes\cdots\vartimes {\mathbb{Z}}_{{n}_{k}}$ sont isomorphes en tant qu'alg�bres de Cayley, et il en est de m�me entre ${\mathbb{Z}}_{n;\alpha}$ et
${\mathbb{Z}}_{{n}_{1};\alpha}\vartimes\cdots\vartimes {\mathbb{Z}}_{{n}_{k};\alpha}$,
entre ${\mathbb{Z}}_{n;\alpha,\beta}$ et
${\mathbb{Z}}_{{n}_{1};\alpha,\beta}\vartimes\cdots\vartimes {\mathbb{Z}}_{{n}_{k};\alpha,\beta}$ et enfin entre
${\mathbb{Z}}_{n;\alpha,\beta,\gamma}$ et
${\mathbb{Z}}_{{n}_{1};\alpha,\beta,\gamma}\vartimes\cdots\vartimes {\mathbb{Z}}_{{n}_{k};\alpha,\beta,\gamma}$.

On d�duit de cela un isomorphisme entre ${\mathbb{Z}}_{n}^{*}$ et
${\mathbb{Z}}_{{n}_{1}}^{*}\vartimes\cdots\vartimes {\mathbb{Z}}_{{n}_{k}}^{*}$,
entre ${\mathbb{Z}}_{n;\alpha}^{*}$ et
${\mathbb{Z}}_{{n}_{1};\alpha}^{*}\vartimes\cdots\vartimes {\mathbb{Z}}_{{n}_{k};\alpha}^{*}$,
entre ${\mathbb{Z}}_{n;\alpha,\beta}^{*}$ et
${\mathbb{Z}}_{{n}_{1};\alpha,\beta}^{*}\vartimes\cdots\vartimes {\mathbb{Z}}_{{n}_{k};\alpha,\beta}^{*}$
et enfin entre ${\mathbb{Z}}_{n;\alpha,\beta,\gamma}^{*}$ et
${\mathbb{Z}}_{{n}_{1};\alpha,\beta,\gamma}^{*}\vartimes\cdots\vartimes {\mathbb{Z}}_{{n}_{k};\alpha,\beta,\gamma}^{*}$.

\end{frame}


\begin{frame}

\frametitle{Entiers de Cayley: le th�or�me d'Euler}

On d�montre classiquement que l'ordre d'un �l�ment de ${\mathbb{Z}}_{n}^{*}$ divise le cardinal de
${\mathbb{Z}}_{n}^{*}$. Il est possible de d�finir de mani�re analogue � ce qui a �t� fait dans le cours la notion
d'ordre d'un entier de Cayley, et il est alors possible de d�montrer que l'ordre d'un �l�ment de
${\mathbb{Z}}_{n;\alpha}^{*}$ divise le cardinal de ${\mathbb{Z}}_{n;\alpha}^{*}$, puis que l'ordre d'un �l�ment de
${\mathbb{Z}}_{n;\alpha,\beta}^{*}$ divise le cardinal de ${\mathbb{Z}}_{n;\alpha,\beta}^{*}$ et enfin que l'ordre d'un
�l�ment de ${\mathbb{Z}}_{n;\alpha,\beta,\gamma}^{*}$ divise le cardinal de ${\mathbb{Z}}_{n;\alpha,\beta,\gamma}^{*}$.

Une des cons�quences de la propri�t� ci-dessus est que le th�or�me d'Euler en arithm�tique modulaire se
g�n�ralise au entiers de Cayley. Ainsi, soit $E$ un ensemble
d'entiers de Cayley (y compris un anneau de congruences), ${E}^{*}$ les �l�ments de
$E$ inversibles pour la multiplication, et $a \in {E}^{*}$, alors ${a}^{\cardinal{({E}^{*})}} = 1$.
Il est important de remarquer qu'ici $({E}^{*},*)$ n'est g�n�ralement pas un groupe!

\end{frame}


\section[info]{Informatique}

\begin{frame}

\begin{center}
{\LARGE
Informatique
}
\end{center}

\end{frame}


\subsection[RSA]{RSA octonionique}

\begin{frame}

\frametitle{RSA \& RSA octonionique: fondement}

\begin{center}
\begin{figure}
\includegraphics[scale=0.35]{rsa-photo}
\caption{Ron Rivest, Adi Shamir and Leonard Adleman}
\end{figure}
\end{center}
L'algorithme \enquote{RSA octonionique} impl�ment� dans ce travail est une \allowbreak g�n�ralisation de
l'algorithme RSA classique. Comme ce dernier, il est fond� sur une exponentiation dans une structure finie.
Plus pr�cis�ment, il utilise la g�n�ralisation pr�c�dement �voqu�e du th�or�me d'Euler.
La diff�rence principale entre ces versions r�side
dans l'encodage des fragments de message, de mani�re � assurer l'inversibilit�.

\end{frame}


\begin{frame}

\frametitle{RSA \& RSA octonionique:  m�thode}

La cl� secr�te est un couple de nombres premiers distincts ${n}_{1}$ et ${n}_{2}$, trois entier $\alpha$, $\beta$ et
$\gamma$ inversibles dans ${\mathbb{Z}}_{{n}_{1}{n}_{2}}$ formant les constantes de structure et d'un exposant priv�, construit ainsi que d�taill� ci-dessous.

La cl� publique est le produit $n = {n}_{1}{n}_{2}$, les constantes $\alpha$, $\beta$ et $\gamma$ et l'exposant public ${e}_{u}$, un entier premier avec le nombre d'inversible des alg�bres de Cayley
${\mathbb{Z}}_{{n}_{1};\alpha,\beta,\gamma}$ et ${\mathbb{Z}}_{{n}_{2};\alpha,\beta,\gamma}$.
L'exposant priv� est alors un entier ${e}_{r}$ tel que
${e}_{r}{e}_{u} \equiv 1 \Module{\cardinal{\left({\mathbb{Z}}_{n;\alpha,\beta,\gamma}^{*}\right)}}$
que l'on calcule par l'algorithme de B�zout, en utilisant le \enquote{th�or�me chinois} pour calculer le nombre
d'inversibles. Il est toujours possible de choisir
$0< {e}_{r} < \cardinal{\left({\mathbb{Z}}_{n;\alpha,\beta,\gamma}^{*}\right)}$
quitte � soustraire un multiple du nombre d'inversibles.

On encrypte le message (encod� dans un octonion inversible) en le mettant � la puissance de l'exposant publique. On d�crypte le cryptogramme en le mettant � la puissance de l'exposant priv�.

RSA peut �tre vue comme un cas particulier de RSA octonionique, o� toutes les composantes du message sont nulles sauf la premi�re.

Une m�me cl� peut �tre utilis�e pour encrypter des informations selon l'un ou l'autre algorithme.

\end{frame}


\begin{frame}[fragile]

\frametitle{RSA \& RSA octonionique:  fragments de code}

Le fragment de code ci-dessous illustre la mani�re d'encoder un fragment de message pour RSA:

\begin{lstlisting}
def encode_RSA(paquet):
		
	un_entier = 0
	
	for b in paquet:
		
		un_entier = un_entier << 8
		
		un_entier += b
	
	return un_entier
\end{lstlisting}

\end{frame}


\begin{frame}[fragile]

\frametitle{RSA \& RSA octonionique:  fragments de code (suite)}

Le fragment de code ci-dessous illustre la mani�re de s'assurer que l'octonion message soit inversible pour \enquote{RSA octonionique}, les autres composantes �tant construites de mani�re similaire � RSA. Le d�codage s'assure que les �l�ments suppl�mentaires sont retir�s avant que le message d�crypt� et d�cod� ne soit �mis.

\begin{lstlisting}
		can_proceed = False
		taille_max = 1 <<(8*longueur)
		
		while not can_proceed:
			
			premiere_composante = structures[0](random.randrange(taille_max))
			norme_tot = norme+(premiere_composante)**2 
				#par propriet� de la norme pour la 1er composante
			can_proceed = norme_tot.is_invertible()
\end{lstlisting}

L'encryption pour \enquote{RSA octonionique} est impl�ment�e comme suit:

\begin{lstlisting}
def encrypte_RSA_octonion (message, exposant_publique):	
	return message**exposant_publique
\end{lstlisting}

\end{frame}


\begin{frame}

\frametitle{RSA \& RSA octonionique:  complexit�}

On comparera les complexit�s temporelles de l'encryption et de la d�cryption en nombres de multiplications modulaires, puisqu'elle est la base des deux algorithmes, qu'une multiplication octonionique est �quivalente � 64 multiplications modulaires, et que la complexit� d'une multiplication modulaire d�pend de l'algorithme choisi, aucun algorithme optimal n'�tant connu � ce jour. RSA et \enquote{RSA octonionique} ont m�me complexit�.

Encodage:
Dans les deux cas, l'encodage commence par la transformation d'une liste d'octets de taille convenable en un entier inf�rieur � $n$. Pour cela, on utilise l'algorithme suivant: on initialise par le premier octet, ensuite de quoi on multiplie le r�sultat temporaire par $2^8$, puis on additionne l'octet suivant. On obtient un temps lin�aire en la longueur pour coder la liste d'octets, soit un temps constant d'encodage d'un octet.
\begin{itemize}
\item{L'entier est envoy� tel quel pour RSA (avec un persillage permettant d'assurer que ledit entier est inversible modulo $n$).}
\item{Pour RSA octonionique, le processus est r�p�t� 7 fois, une huiti�me composante de persillage est ajout�e pour assurer l'inversibilit� de l'octonion, puis la liste d'entiers est convertie en un octonion en temps constant (construction d'un objet).}
\end{itemize}

\end{frame}


\begin{frame}

\frametitle{RSA \& RSA octonionique:  complexit� (suite)}

Encryption:
\begin{itemize}
\item{Pour RSA avec l'algorithme d'exponentiation rapide, est en $O\left(\log\left({{e}_{u}}\right)\right)$
multiplications modulo $n$.}
\item{Pour RSA octonionique, l'algorithme d'exponentiation rapide fonctionnant avec les octonions, et puisqu'une multiplication octonionique est en $O$ d'une multiplication modulaire, l'encryption est �galement en
$O\left(\log\left({{e}_{u}}\right)\right)$ multiplications modulaires.}
\end{itemize}

D�cryption:
Dans les deux cas, le cryptogramme est �lev� � la puissance ${e}_{r}$.
\begin{itemize}
\item{Pour RSA avec l'algorithme d'exponentiation rapide, est en $O\left(\log\left({{e}_{r}}\right)\right)$
donc au pire $O\left(\log\left(n\right)\right)$.}
\item{Pour RSA octonionique, le nombre d'inversibles est en ${n}^{8}$, donc la complexit� est �galement born�e
par $O\left(\log\left(n\right)\right)$.}
\end{itemize}

\end{frame}


\subsection[ODC]{ODC}

\begin{frame}

\frametitle{ODC: fondement et m�thode}

ODC est un algorithme � cl� secr�te, dont le fonctionnement est assur� par la di-associativit� du produit des entiers de Cayley.

La cl� est constitu�e de deux octonions inversibles ${o}_{1}$ et ${o}_{1}$, de pr�f�rence ne commutant pas entre eux.

L'encryption consiste � multiplier le fragment de message encod� sous forme d'octonion (non n�cessairement inversible) � gauche par le premier octonion de la cl�, puis � multiplier ce premier produit � droite par le second octonion de la cl�. L'ordre des multiplications est important!

\[C = \left({o}_{1}*M\right)*{o}_{2}\]

La d�cryption consiste � multiplier le fragment de cryptogramme � droite par l'inverse du second octonion de la cl�, puis � multiplier � gauche ce premier produit par l'inverse du premier octonion de la cl�. L'ordre des multiplications est important!

\[M = {o}_{1}^{-1}*\left(C*{o}_{2}^{-1}\right)\]

\end{frame}


\begin{frame}[fragile]

\frametitle{ODC: fragments de code}

Le c{\oe}ur de l'algorithme ODC est constitu� par les fonctions d'encryption et de d�cryption, impl�ment�es de la mani�re suivante:

\begin{lstlisting}
def encrypte_odc(message, cl�_odc):
	return (cl�_odc[0]*message)*cl�_odc[1]

def decrypte_odc(cryptogramme,inverse_cl�):
	return inverse_cl�[0]*(cryptogramme*inverse_cl�[1])
\end{lstlisting}

\end{frame}


\begin{frame}

\frametitle{ODC: complexit�}

ODC utilise deux multiplications octonioniques, chaque multiplication octonionique correspondant � 64 multiplications modulaires. Une multiplication dans ${\mathbb{Z}}_{n}$, lorsque faite de la mani�re la plus na�ve qui soit, �tant de complexit� $O\left({n}^{2}\right)$, il en est donc de m�me pour ODC.

Il avait �t� pr�vu de comparer, en mati�re de complexit�, cet algorithme avec AES, mais ce dernier imposant la taille des cl�s cela s'est av�r� impossible. L'�tude de l'algorithme fondateur d'AES, Rijndael a �t� entreprise, mais n'a pas pu �tre conclue � temps.

\end{frame}


\section[Motivation]{Conclusion}

\begin{frame}

\begin{center}
{\LARGE
Conclusion
}
\end{center}

\end{frame}


\begin{frame}

\frametitle{Conclusion}


\begin{itemize}
\item{Am�lioration du code.}
\item{�tude des attaques.}
\end{itemize}

\end{frame}


\end{document}
