#!/usr/bin/env python3
"""
Demo_octo_dev

Invoquer avec: python3 chemin_complet_de_ce_script …

Paramètres positionnels (obligatoires):

exemple:		Chemin complet d'un fichier servant d'exemple.

cryptogramme:	Chemin complet du cryptogramme correspondant à exemple.

decryption:		Chemin complet du message decrypté correspondant à exemple.

Paramètres avec mot-clef (optionels):

reproductible:	Graine des générateurs aléatoires fixée

Ce script lance une démonstration des différents éléments implémentés.
"""

import os
import sys
import argparse
import random
import copy

import cayley_dickson
import octonions
import crypto_config
import coeur_RSA_octonionique
import coeur_octo_doublecross as coeur_odc

# Utilitaires

sys.stdout = open(1, 'w', encoding='utf-8', closefd=False)	# Pour sortie en Unicode sans peine

def test_octonion( ):
	grand_nombre = 123456789
	constante_de_structure_1 = 1
	constante_de_structure_2 = 1
	constante_de_structure_3 = 1
	print("grand nombre: ", grand_nombre, "\n premiere constante de structure:",
		constante_de_structure_1, "\n deuxième constante de structure: ",
		constante_de_structure_2, "\n troisième constante de structure: ",
		constante_de_structure_3)
	print("test 'paquet_structure_octo' ")
	paquet_structure = octonions.paquet_structure_octo(grand_nombre, constante_de_structure_1, constante_de_structure_2, constante_de_structure_3)
	
	print( "les élement du paquet sont", *paquet_structure)
	
	print(" test de octonion_CD:")
	composante_1 = 87654321
	composante_2 = 7654321
	composante_3 = 654321
	composante_4 = 54321
	composante_5 = 4321
	composante_6 = 321
	composante_7 = 21
	composante_8 = 1
	
	tuple_0 = (composante_1,composante_2,composante_3,composante_4,composante_5,composante_6,composante_7,composante_8)
	
	print(" création d'un octonion de composante", composante_1, composante_2,
		composante_3,composante_4,composante_5,composante_6,composante_7,
		composante_8, sep = " ")
	octonion_1 = octonions.octonion_CD(paquet_structure,composante_1,composante_2,composante_3,
		composante_4,composante_5,composante_6,composante_7,composante_8)
	tuple_1 = octonion_1.flatten()
# 	composante_1b = tuple_1[0]
# 	composante_2b = tuple_1[1]
# 	composante_3b = tuple_1[2]
# 	composante_4b = tuple_1[3]
# 	composante_5b = tuple_1[4]
# 	composante_6b = tuple_1[5]
# 	composante_7b = tuple_1[6]
# 	composante_8b = tuple_1[7]
	
	verif = (tuple_0 == tuple_1)
	print ("Ça marche? ", verif)
	
	print(" test de octonion_tuple avec un octonion de même composante")
	octonion_2 = octonions.octonion_tuple(paquet_structure,tuple_0)
	
	verif_2 = (octonion_1 == octonion_2)
	
	print ("Ça marche? ", verif_2) 
	 
	
def test_coeur_RSA_octo():
	
	print("Test du cœur RSA octonionique \n")
	
	exposant_publique = 11
	taille_paquet = 3
	
	print("Test 'generateur_clé_prive'\n")
	
	cle_privee = coeur_RSA_octonionique.generateur_cle_privee_octonion(exposant_publique, taille_paquet,1,1,1, reproductible = crypto_config.reproductibilité)
	
	print("Clef privée: ", cle_privee, "\n")
	
	print("Test 'trouve_exposant_prive'\n")
	
	seeking = True
	
	exposant_prive = coeur_RSA_octonionique.trouve_exposant_prive_octonion(cle_privee, exposant_publique)
		
	print("exposant privée: ", exposant_prive, "\n")
	
	print("Test 'generateur_cle_publique'\n")
	
	cle_publique = coeur_RSA_octonionique.generateur_cle_publique_octonion(cle_privee, exposant_publique)
	
	print("Clef publique: ", cle_publique)
	
	paquet_structure = octonions.paquet_structure_octo(*cle_publique[:4])
	
	# message = octonions.octonion_CD(paquet_structure,12345678, 12345687,12345876,12348765,12387654,12876543) #le dernier nombre est omie (=0)
# 	
# 	print("octonion à communiquer: ", message.flatten())
	
	#le message doit être une séquence transformé en octonion par encode_RSA qui produit un octonion >inversible< sans padding possible
	
	information = (12345678, 12345687,12345876,12348765,12387654,12876543, 18765432)
	
	print("information à communiquer:",*information, sep = ', ')
	
	print ("Test 'encode_RSA_octo' ")
	
	message = coeur_RSA_octonionique.encode_RSA_octo(information,paquet_structure, reproductible = crypto_config.reproductibilité)
	
	print("Test 'encrypte_RSA_octonion'\n")
	
	cryptogramme = coeur_RSA_octonionique.encrypte_RSA_octonion(message, cle_publique[4])
	print("cryptogramme:", cryptogramme, "\n")
	
	print("Test 'decrypte_RSA_octonion'\n")
	
	decryption = coeur_RSA_octonionique.decrypte_RSA_octonion(cryptogramme, exposant_prive)
	
	print("decryption: ", decryption.flatten(), "\n")
	
	print("Ça marche? ", information == decryption.flatten()[1:])
	
	print("\n")


def test_tout_RSA_octo(chemin_message, chemin_cryptogramme, chemin_decryption):
	
	taille_composante = 3
	taille_à_lire = taille_composante*7
	exposant_publique = 11
	nombre_padding = 0
	cle_privee = coeur_RSA_octonionique.generateur_cle_privee_octonion(exposant_publique, taille_composante,1,1,1, reproductible = crypto_config.reproductibilité)
	cle_publique = coeur_RSA_octonionique.generateur_cle_publique_octonion(cle_privee, exposant_publique)
	with open(chemin_message, "rb") as fp,\
		open(os.path.join(chemin_commun, chemin_cryptogramme), "w", newline = None) as cg:
		
		paquet_structure = octonions.paquet_structure_octo(*cle_publique[:4])
		
		encryption_fini = False
		
		while not encryption_fini:
			
			lu = fp.read(taille_à_lire)
			
			if len(lu) != taille_à_lire:
				
				encryption_fini = True
				
				nombre_padding = taille_à_lire - len(lu)
				
				lu += bytes(nombre_padding)
				
				
			
			
			lu_encodée_octo = coeur_RSA_octonionique.encode_RSA_octo(lu,paquet_structure, reproductible = crypto_config.reproductibilité)
			
			cryptogramme= coeur_RSA_octonionique.encrypte_RSA_octonion(lu_encodée_octo,cle_publique[4])
			
			cg.write(str(cryptogramme.flatten())+"\n")
	
	exposant_prive =coeur_RSA_octonionique.trouve_exposant_prive_octonion(cle_privee, exposant_publique)
		
	with open(os.path.join(chemin_commun, chemin_cryptogramme), "r") as cg,\
		open(os.path.join(chemin_commun, chemin_decryption), "wb") as dc:
		
		paquet_structure = octonions.paquet_structure_octo(*cle_publique[:4])
		
		buffer = None
		
		for line in iter(cg.readline, ''):
			
			if buffer is not None:
				liste_brute = buffer.split(sep = ",")
				liste_brute[0] = liste_brute[0][1:]
				liste_brute[-1] = liste_brute[-1][0:-2]
				
				liste_int = [int(i) for i in liste_brute]
				reoctonion = octonions.octonion_tuple(paquet_structure, liste_int)
				decryption =  coeur_RSA_octonionique.decrypte_RSA_octonion(reoctonion, exposant_prive)
				paquet = coeur_RSA_octonionique.decode_RSA_octo(decryption, taille_composante)
				dc.write(bytes(paquet))

			buffer = copy.copy(line)
		
		if buffer is not None:
			liste_brute = buffer.split(sep = ",")
			liste_brute[0] = liste_brute[0][1:]
			liste_brute[-1] = liste_brute[-1][0:-2]
			
			liste_int = [int(i) for i in liste_brute]
			reoctonion = octonions.octonion_tuple(paquet_structure, liste_int)
			decryption =  coeur_RSA_octonionique.decrypte_RSA_octonion(reoctonion, exposant_prive)
			paquet_brute = coeur_RSA_octonionique.decode_RSA_octo(decryption, taille_composante)
			
			paquet_émondé = paquet_brute[:taille_à_lire - nombre_padding]
			dc.write(bytes(paquet_émondé))
			
		
# 		for line in iter(cg.readline, ''): #TODO: suprimer les pad
# 			
# 			liste_brute = line.split(sep = ",")
# 			liste_brute[0] = liste_brute[0][1:]
# 			liste_brute[-1] = liste_brute[-1][0:-2]
# 			
# 			liste_int = [int(i) for i in liste_brute]
# 			reoctonion = octonions.octonion_tuple(paquet_structure, liste_int)
# 			decryption =  coeur_RSA_octonionique.decrypte_RSA_octonion(reoctonion, exposant_prive)
# 			paquet = coeur_RSA_octonionique.decode_RSA_octo(decryption, taille_composante)
# 			dc.write(bytes(paquet))


def test_coeur_octo_doublecross():
	print("Test du cœur octo doublecross octonionique \n")
	taille_composante = 4
	
	print ("Test clé_odc")
	clé_odc = coeur_odc.generateur_clé_odc(taille_composante, reproductible= crypto_config.reproductibilité)
	print("Clef odc: ", clé_odc, "\n")
	
	information = (12345678, 12345687,12345876,12348765,12387654,12876543, 18765432, 87654321)
	print("information à communiquer:",*information, sep = ', ')
	#print("information à communiquer: ", information)
	
	print( "Test 'creer_paquet_structure_ocd'")
	paquet_structure = coeur_odc.creer_paquet_structure_ocd(clé_odc)
	
	print ("Test 'encode_odc'\n")
	message = coeur_odc.encode_odc(information,paquet_structure)
	
	print("Test 'encrypte_odc'\n")
	cryptogramme = coeur_odc.encrypte_odc(message,clé_odc)
	print ("cryptogramme: ", cryptogramme.flatten())
	
	print ("Test 'inverseur_clé_odc'\n")
	inverse_clé = coeur_odc.inverseur_clé_odc(clé_odc)
	print("inverse de la clé: ", inverse_clé, "\n")
	print("Ça inverse bien? ", inverse_clé[0]*clé_odc[0] == paquet_structure[3].upcast(1), ", ", inverse_clé[1]*clé_odc[1]==paquet_structure[3].upcast(1), "\n")
	
	print("Test 'decrypte'\n")
	decryption = coeur_odc.decrypte_odc(cryptogramme,inverse_clé)
	print("decryption: ", decryption.flatten(), "\n")
	
	print("Ça marche? ", information == decryption.flatten())
	print("\n")

def  test_tout_double_octo_doublecross(chemin_message, chemin_cryptogramme, chemin_decryption):
	
	taille_composante = 3
	taille_à_lire = taille_composante*8
	
	nombre_padding = 0
	cle_odc = coeur_odc.generateur_clé_odc(taille_composante, reproductible= crypto_config.reproductibilité)
	
	with open(chemin_message, "rb") as fp,\
		open(os.path.join(chemin_commun, chemin_cryptogramme), "w", newline = None) as cg:
		
		paquet_structure = coeur_odc.creer_paquet_structure_ocd(cle_odc)
		
		encryption_fini = False
		
		while not encryption_fini:
			
			lu = fp.read(taille_à_lire)
			
			if len(lu) != taille_à_lire:
				
				encryption_fini = True
				
				nombre_padding = taille_à_lire - len(lu)
				
				lu += bytes(nombre_padding)
				

			lu_encodée_octo =  coeur_odc.encode_odc(lu,paquet_structure)
			cryptogramme = coeur_odc.encrypte_odc(lu_encodée_octo,cle_odc)
			
			cg.write(str(cryptogramme.flatten())+"\n")
			
		
	with open(os.path.join(chemin_commun, chemin_cryptogramme), "r") as cg,\
		open(os.path.join(chemin_commun, chemin_decryption), "wb") as dc:
		
		paquet_structure = coeur_odc.creer_paquet_structure_ocd(cle_odc)
		inverse_clé = coeur_odc.inverseur_clé_odc(cle_odc)
		
		buffer = None
		
		for line in iter(cg.readline, ''):
			
			if buffer is not None:
				liste_brute = buffer.split(sep = ",")
				liste_brute[0] = liste_brute[0][1:]
				liste_brute[-1] = liste_brute[-1][0:-2]
				
				liste_int = [int(i) for i in liste_brute]
				reoctonion = octonions.octonion_tuple(paquet_structure, liste_int)
				decryption =  coeur_odc.decrypte_odc(reoctonion,inverse_clé)
				paquet = coeur_odc.decode_odc(decryption,taille_composante)
				dc.write(bytes(paquet))
				
		

			buffer = copy.copy(line)
		
		if buffer is not None:
			liste_brute = buffer.split(sep = ",")
			liste_brute[0] = liste_brute[0][1:]
			liste_brute[-1] = liste_brute[-1][0:-2]
			
			liste_int = [int(i) for i in liste_brute]
			reoctonion = octonions.octonion_tuple(paquet_structure, liste_int)
			decryption =  coeur_odc.decrypte_odc(reoctonion,inverse_clé)
			paquet_brute = coeur_odc.decode_odc(decryption,taille_composante)
			
			paquet_émondé = paquet_brute[:taille_à_lire - nombre_padding]
			dc.write(bytes(paquet_émondé))
			
		







def test_tout(chemin_message, chemin_cryptogramme, chemin_decryption):
	
	print("\n"+"Ensemble des tests\n")
	
	test_octonion()
	test_coeur_RSA_octo()
	test_tout_RSA_octo(chemin_message, chemin_cryptogramme, chemin_decryption)
	test_coeur_octo_doublecross()
	test_tout_double_octo_doublecross(chemin_message, chemin_cryptogramme, chemin_decryption)


# CLI exploitation

chemin_commun = os.path.dirname(sys.argv[0])

parser = argparse.ArgumentParser(description = "Ce script lance une "+
	"démonstration des différents éléments implémentés.")

parser.add_argument("exemple", help="Chemin complet d'un fichier servant d'exemple.",
	type = str)

parser.add_argument("cryptogramme", help="Chemin complet du cryptogramme correspondant à exemple.",
	type = str)

parser.add_argument("decryption", help="Chemin complet du message decrypté correspondant à exemple.",
	type = str)

parser.add_argument("--reproductible", help="Graine des générateurs aléatoires fixée",
	type = bool, default = False)

args = parser.parse_args()

chemin_exemple = args.exemple

chemin_cryptogramme = args.cryptogramme

chemin_decryption = args.decryption

crypto_config.reproductibilité = args.reproductible


# Schmurf

# Gestion de la reproductibilité

if crypto_config.reproductibilité:# and not générateurs_initialisés:
	
	random.seed(crypto_config.graine_des_générateurs_aléatoires)
	
	#générateurs_initialisés = True

# Tests

test_tout(chemin_exemple, chemin_cryptogramme, chemin_decryption)


# In the end, there can be None

sys.exit(0)
