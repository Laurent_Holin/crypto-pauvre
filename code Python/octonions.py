"""
octonions
bibliothèque spécifiquement pour les octonions, pour certaines manipulations 
et pour simplifier la syntaxe
"""

import congruence_rings
import cayley_dickson

# on peut pas spécifier le contenu des variables
# def complexe_CD ( (num1, num2), (annneau_de_base,anneau_doublé))
# 	""" renvoie le complexe de Cayley-Dickson de composante num1 et num2
# 		dans l'anneau annneau_de_base
# 	"""
# 
# 	return anneau_doublé(annneau_de_base(num1), anneau_de_base(num2))
# 
# def quaternion_CD ( (num1, num2, num3, num4) , (annneau_de_base,anneau_doublé, anneau_quadruplé))
# 	"""
# 	renvoie le quaternion de Cayley-Dickson de composante num1, num2, num3 et num4
# 		dans l'anneau annneau_de_base
# 	"""
# 	complexe1 = complexe_CD ( (num1, num2), (annneau_de_base,anneau_doublé))
# 	complexe2= complexe_CD ( (num3, num4), (annneau_de_base,anneau_doublé))
# 	return anneau_quadruplé(complexe1, complexe2)
# 	
# 	
# 	
# def octonion_CD ( (num1, num2, num3, num4, num5, num6, num7, num8), (annneau_de_base,anneau_doublé, anneau_quadruplé, anneau_octonions)):
# 	quaternion1 = quaternion_CD ( (num1, num2, num3, num4), (annneau_de_base,anneau_doublé, anneau_quadruplé))
# 	quaternnion2= quaternion_CD (( num5, num6, num7, num8), (annneau_de_base,anneau_doublé, anneau_quadruplé))
# 	return anneau_octonions( quaternion1, quaternnion2)

def paquet_structure_octo (congruence, constante_de_structure1 =1, constante_de_structure2 =1, constante_de_structure3=1):
	"""
	revoie un tuple contenant  l'anneau de congruence de "congruence" et
	trois doublementssuccessifs de Cayley-Dickson de ce dernier
	en utilisant les constantes de structures spécifier
	
	"""
	
	anneau_cong = congruence_rings.congruence_ring(congruence)
	doublement_1 = cayley_dickson.cayley_dickson_doubling(anneau_cong, anneau_cong(constante_de_structure1))
	doublement_2 = cayley_dickson.cayley_dickson_doubling(doublement_1,anneau_cong(constante_de_structure2))
	octonion = cayley_dickson.cayley_dickson_doubling(doublement_2,anneau_cong(constante_de_structure3))
	return (anneau_cong,doublement_1,doublement_2,octonion)


def octonion_CD ( paquet, num1 = 0, num2 = 0, num3=0, num4=0, num5=0, num6=0, num7=0, num8=0):
	"""
 	renvoie l'octonion de Cayley-Dickson de composante num1, num2, num3, num4,
 	num5, num6, num7,, num8 dans l'anneau paquet[3], les anneaux paquet[0] ,
 	paquet[1], paquet[2] étant les anneaux avant doublements
 	"""
	
	return paquet[3](paquet[2](paquet[1](paquet[0](num1),paquet[0](num2)),paquet[1](paquet[0](num3),paquet[0](num4))),
		paquet[2](paquet[1](paquet[0](num5),paquet[0](num6)),paquet[1](paquet[0](num7),paquet[0](num8))))


def octonion_tuple(paquet,num): #peut-être changer son nom en octonion_iter, par se que ça marche avec toutes les sequences
	"""
	idem octonion_CD, sauf que les composantes sont dans une séquence plutôt qu'entré manuellement
	"""
	return paquet[3](paquet[2](paquet[1](paquet[0](num[0]),paquet[0](num[1])),paquet[1](paquet[0](num[2]),paquet[0](num[3]))),paquet[2](paquet[1](paquet[0](num[4]),paquet[0](num[5])),paquet[1](paquet[0](num[6]),paquet[0](num[7]))))
	#just add brackets

