Most of today's cryptographic algorithms work on algebraic structures with
strong properties (commutativity, associativity…), mostly groups and
commutative rings (congruence rings and elliptic curves).

These properties allow more flexibility to design cryptographic primitives, but
also give tools to those trying to break them.

The idea is then to find algebraic structures just rich enough to allow for
cryptographic primitives.

I use here algorithms from the article "Impoverished Cryptography"
[doi: 10.13140/RG.2.2.24952.55040], which uses Cayley algebras, and of these a
special kind termed "Cayley Integers" built from congruence rings through a
doubling procedure (described in the presentation).

I have implemented some of those algorithms in their octonionic version
(8-component Cayley Integers) and have also investigated their theoretical time
complexity.

We make use in this code of the Cayley-Dickson library available at
https://github.com/HubertHolin/Cayley-Dickson
