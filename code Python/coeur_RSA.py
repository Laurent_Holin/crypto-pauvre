#!/usr/bin/env python3
"""
coeur_RSA

Ce fichier imlémente le cœur de l'encryption et décryption de l'algorithme RSA.
"""

import os
import random
import secrets

import euclide_etendu

import crypto_config


liste_premiers = []


def charge_liste_premiers(min, max, exposant_publique):
	
	#C.f. https://stackoverflow.com/questions/5137497/find-current-directory-and-files-directory
	
	ce_chemin = os.path.dirname(os.path.realpath(__file__))
	
	liste_premiers = []
	
	for fichier in ["primes1.txt", "primes2.txt", "primes3.txt"]:
		
		chemin_fichier_premiers = os.path.join(ce_chemin, "Premiers", fichier)
		
		if not os.path.exists(chemin_fichier_premiers):
			
			raise RuntimeError("Fichier "+fichier+" non trouvé!")
		
		with open(chemin_fichier_premiers, "r") as fp:
			
			saut = False
			
			for line in iter(fp.readline, ''):
				
				if not saut:
					
					saut = True
					
					continue
				
				for chaine_premier in line.split():
					
					nb_premier = int(chaine_premier)
					
					if nb_premier >= min and nb_premier%exposant_publique !=1: # car sinon phy(nb_premier) et exposant_publique ne sont pas premier entre eux
						
						liste_premiers.append(nb_premier)
					
					if nb_premier >= max:
						
						return liste_premiers
	
	return liste_premiers


def encrypte_RSA(message , exposant, grand_nombre):
	"""
	"""
	
	return pow(message , exposant, grand_nombre)


def decrypte_RSA(message , exposant, grand_nombre):
	
	return encrypte_RSA(message , exposant, grand_nombre)


def generateur_cle_publique(cle_privee, exposant_publique):
	"""
	Cette fonction renvoie un tuple (grand_nombre, exposant_publique)
	"""
	
	grand_nombre = cle_privee[0] * cle_privee[1]
	
	return (grand_nombre, exposant_publique)


def generateur_cle_privee(exposant_publique, taille_paquet):	#Finir
	"""
	Renvoie un couple de nombre premier constituant la clès privée.
	Attention, il faut que exposant_publique soit premier (ceci n'est pas testé)!
	"""
	
	# TODO: choisir aléatoirement deux nombre premiers *distincts* de bonne_liste
	# le produit des nombres doit être < 2^32 et par exemple chaque nombre >= 2^10
	
	bonne_liste = charge_liste_premiers((1<<(2*taille_paquet)),(1<<(7*taille_paquet)) ,exposant_publique )
	#bonne_liste = [nb for nb in liste_premiers if (nb-1) % exposant_publique != 0]
	
	longueur = len(bonne_liste)
	
	if crypto_config.reproductibilité:
		
		premier_tirage = random.randrange(longueur)
		premier_nombre = bonne_liste[premier_tirage]
		
		#Refair une liste en supprimant premier_tirage,
		#puis en cherchant la borne du slice
		#Amélioration possible: recherche dichotomique plutôt qu'exaustive.
		del bonne_liste[premier_tirage]
		
		taille_min = (1<<(8*taille_paquet))//premier_nombre
		
		indice_bas = 0
		
		while (bonne_liste[indice_bas] < taille_min and indice_bas < len(bonne_liste)-1):
			
			indice_bas +=1
		
		taile_max = (1<<(9*taille_paquet))//premier_nombre
		
		indice_haut = indice_bas-1
		
		while (bonne_liste[indice_haut] < taile_max and indice_haut < len(bonne_liste)-1):
			
			indice_haut +=1
		
		nouvelle_liste = bonne_liste[indice_bas : indice_haut]
		
		if len(nouvelle_liste) == 0:
			
			return generateur_cle_privee(exposant_publique, taille_paquet)
		
		second_nombre = random.choice(nouvelle_liste)
		
		return (premier_nombre, second_nombre)
	
	else:
		
		premier_tirage = secrets.randbelow(longueur-1)
		#print(premier_tirage)
		premier_nombre = bonne_liste[premier_tirage]
		
		del bonne_liste[premier_tirage]
		
		#taille_min = (1<<(8*taille_paquet))//premier_nombre
		
		indice_bas = 0
		
		#while (bonne_liste[indice_bas] < taille_min and indice_bas < len(bonne_liste)-1):
		while (premier_nombre*bonne_liste[indice_bas] < (1<<(8*taille_paquet)) and indice_bas < len(bonne_liste)-1):
			
			indice_bas +=1
		
		#taile_max = (1<<(9*taille_paquet))//premier_nombre
		
		indice_haut = indice_bas-1
		
		#while (bonne_liste[indice_haut] < taile_max and indice_haut < len(bonne_liste)-1):
		while (premier_nombre*bonne_liste[indice_haut] < (1<<(9*taille_paquet)) and indice_haut < len(bonne_liste)-1):
			
			indice_haut +=1
		
		nouvelle_liste = bonne_liste[indice_bas : indice_haut]
		
		if len(nouvelle_liste) == 0:
			
			return generateur_cle_privee(exposant_publique, taille_paquet)
		
		second_nombre = secrets.choice(nouvelle_liste)	# TODO: s'assurer qu'il y en ait un
		
		return (premier_nombre, second_nombre)



def trouve_exposant_prive(cle_privee, expo_publique):
	"""
	"""
	
	phy_n = (cle_privee[0] - 1)*(cle_privee[1] -1)
	paquet = euclide_etendu.euclide_etendu(expo_publique, phy_n)
	
	exposant_prive = paquet[0]
	
	if exposant_prive < 0:
		
		exposant_prive += phy_n
	
	return exposant_prive


def encode_RSA(paquet):
	
# 	octet_par_octet = [bytes([b]) for b in chunk]
# 	
# 	dump = ""
# 	
# 	for toto in octet_par_octet:
# 		
# 		dump += toto.hex()
# 		dump += " " 
# 	
# 	print(dump)
	
	un_entier = 0
	
	for b in paquet:
		
		un_entier = un_entier << 8
		
		un_entier += b
	
	return un_entier


def encode_RSA_junked (paquet , nb_poubelle):
	if crypto_config.reproductibilité:
		entier = random.randrange(1<<(8*nb_poubelle) -1)
		for b in paquet:
			
			un_entier = un_entier << 8
			
			un_entier += b
		
		return un_entier
	
	else:	
		entier = secrets.randbelow(1<<(8*nb_poubelle))
		
		for b in paquet:
			
			un_entier = un_entier << 8
			
			un_entier += b
		
		return un_entier
	



def decode_RSA_naif(un_entier):
	
	res = []
	
	while un_entier > 0:
		
		#res.append(un_entier % 256)
		res.append(un_entier & 0xFF)	#rique de créer un bug si il y a un octect nul
		
		un_entier >>= 8
	
	return bytes(reversed(res))


def decode_RSA (un_entier):
	
	return decode_RSA_naif(un_entier)


def decode_RSA_junked (entier , taille_paquet):
	res = []
	
	for i in range(taille_paquet):
		res.append(un_entier & 0xFF)
		
		un_entier >>= 8
	
	return bytes(reversed(res))















