#!/usr/bin/env python3
"""
invertibles_test

Invoque with: python3 path_to_this_script


Positional (mandatory) argument:

Keyword (optional) arguments:


This script tests the number of invetibles in objects created via the Cayley-Dickson
doubling procedure.
"""


import os
import sys
import argparse
import codecs
import datetime
import itertools


import arithmetic
import congruence_rings
import cayley_dickson


# Constants definitions

inch = 2.54					# in cm

minutes_in_a_day = 1440

absolute_zero = -273.15		# in °C

outputs_top_storage_name = "Outputs"

fig_extension = "pdf"

a_few_tests = {
	2:	[1, 2, 3, 4],
	3:	[1, 2, 3],
	5:	[1, 2],
	7:	[1, 2],
	11:	[1],
	13:	[1]}

# Utility objects and functions


# CLI exploitation

common_path = os.path.dirname(sys.argv[0])

this_script_name = os.path.splitext(os.path.basename(sys.argv[0]))[0]

parser = argparse.ArgumentParser(description = "This script tests the number of "+
	"invetibles in objects created via the Cayley-Dickson doubling procedure.")

args = parser.parse_args()


# Create output folders

top_outputs_path = os.path.join(common_path, outputs_top_storage_name)

if not os.path.exists(top_outputs_path):
	os.mkdir(top_outputs_path)

outputs_path = os.path.join(top_outputs_path, this_script_name)

if not os.path.exists(outputs_path):
	os.mkdir(outputs_path)


# Schmurf

with codecs.open(os.path.join(outputs_path, "matchmaker"+".txt"), 'w', "utf-8-sig")\
	as matchmaker:
	
	for a_prime in a_few_tests:
		
		print("Prime: ", a_prime)
		matchmaker.write("Prime: "+str(a_prime))
		
		print("\n")
		matchmaker.write("\n")
		
		for an_exponent in a_few_tests[a_prime]:
			
			print("\tExponent: ", an_exponent)
			matchmaker.write("\tExponent: "+str(an_exponent))
			
			print("\n")
			matchmaker.write("\n")
			
			# Test for the base ring
			
			print("\t\tBase ring:")
			matchmaker.write("\t\tBase ring:\n")
			
			the_base_ring = congruence_rings.congruence_ring(a_prime**an_exponent)
			
			if a_prime == 2:
				
				predicted_base = 2**(an_exponent-1)
			
			else:
				
				predicted_base = (a_prime-1)*(a_prime**(an_exponent-1))
			
			base_before = datetime.datetime.now()
			
			nb_of_invertibles_base = 0
			
			for an_integer in range(a_prime**an_exponent):
				
				an_element_in_the_base_ring = the_base_ring(an_integer)
				
				if an_element_in_the_base_ring.is_invertible():
					
					nb_of_invertibles_base += 1
			
			base_after = datetime.datetime.now()
			
			print("\t\t\tfound {0:d} - expected {1:d} - ".format(nb_of_invertibles_base, predicted_base)+
				("Match!" if (nb_of_invertibles_base == predicted_base) else "Hello Houston…"))
			matchmaker.write("\t\t\tfound {0:d} - expected {1:d} - ".format(nb_of_invertibles_base, predicted_base)+
				("Match!" if (nb_of_invertibles_base == predicted_base) else "Hello Houston…"))
			print("\t\t\tcomputation time: ", base_after-base_before)
			
			print("\n")
			matchmaker.write("\n")
			
			# Test for the first doubling
			
			for a_first_constant in range(a_prime**an_exponent):
				
				if a_first_constant % a_prime == 0:	# Left as an exercise!
					
					continue
				
				print("\t\t\tFirst doubling: {0:d}".format(a_first_constant))
				matchmaker.write("\t\t\tFirst doubling: {0:d}\n".format(a_first_constant))
				
				the_first_doubling =\
					cayley_dickson.cayley_dickson_doubling(the_base_ring, the_base_ring(a_first_constant))
				
				if a_prime == 2:
					
					predicted_first = 2**(2*an_exponent-1)
				
				else:
					
					factor_first = arithmetic.Jacobi(a_first_constant % a_prime, a_prime)
					factor_first *= (1 if (((a_prime-1)>>1) % 2 == 0) else -1)
					
					predicted_first = a_prime**(2*an_exponent)
					predicted_first -= (a_prime+(a_prime-1)*factor_first)*a_prime**(2*(an_exponent-1))
				
				first_before = datetime.datetime.now()
				
				nb_of_invertibles_first = 0
				
				for first_component in range(a_prime**an_exponent):
					
					for second_component in range(a_prime**an_exponent):
						
						an_element_in_the_first_doubling =\
							the_first_doubling(the_base_ring(first_component), the_base_ring(second_component))
						
						if an_element_in_the_first_doubling.is_invertible():
							
							nb_of_invertibles_first += 1
				
				first_after = datetime.datetime.now()
				
				print("\t\t\t\tfound {0:d} - expected {1:d} - ".format(nb_of_invertibles_first, predicted_first)+
					("Match!" if (nb_of_invertibles_first == predicted_first) else "Hello Houston…"))
				matchmaker.write("\t\t\t\tfound {0:d} - expected {1:d} - ".format(nb_of_invertibles_first, predicted_first)+
					("Match!" if (nb_of_invertibles_first == predicted_first) else "Hello Houston…"))
				print("\t\t\t\tcomputation time: ", first_after-first_before)
				
				print("\n")
				matchmaker.write("\n")
				
				# Test for the second doubling
				
				for a_second_constant in range(a_prime**an_exponent):
					
					if a_second_constant % a_prime == 0:	# Left as an exercise!
						
						continue
					
					if a_second_constant < a_first_constant:	# Known by symmetry.
						
						continue
					
					print("\t\t\t\tSecond doubling: {0:d}, {1:d}".format(a_first_constant, a_second_constant))
					matchmaker.write("\t\t\t\tSecond doubling: {0:d}, {1:d}\n".format(a_first_constant, a_second_constant))
					
					the_second_doubling =\
						cayley_dickson.cayley_dickson_doubling(the_first_doubling, the_base_ring(a_second_constant))
					
					if a_prime == 2:
						
						predicted_second = 2**(4*an_exponent-1)
					
					else:
						
						predicted_second = a_prime**(4*an_exponent)
						predicted_second -= (a_prime**3+(a_prime-1)*a_prime)*a_prime**(4*(an_exponent-1))
					
					second_before = datetime.datetime.now()
					
					nb_of_invertibles_second = 0
					
					for first_component, second_component, third_component, fourth_component in\
						itertools.product(range(a_prime**an_exponent), repeat = 4):
						
						an_element_in_the_second_doubling =\
							the_second_doubling(the_first_doubling(the_base_ring(first_component), the_base_ring(second_component)),
							the_first_doubling(the_base_ring(third_component), the_base_ring(fourth_component)))
						
						if an_element_in_the_second_doubling.is_invertible():
							
							nb_of_invertibles_second += 1
					
					second_after = datetime.datetime.now()
					
					print("\t\t\t\t\tfound {0:d} - expected {1:d} - ".format(nb_of_invertibles_second, predicted_second)+
						("Match!" if (nb_of_invertibles_second == predicted_second) else "Hello Houston…"))
					matchmaker.write("\t\t\t\t\tfound {0:d} - expected {1:d} - ".format(nb_of_invertibles_second, predicted_second)+
						("Match!" if (nb_of_invertibles_second == predicted_second) else "Hello Houston…"))
					print("\t\t\t\t\tcomputation time: ", second_after-second_before)
					
					print("\n")
					matchmaker.write("\n")
					
					# Test for the third doubling
					
					if ((a_prime != 2) and (an_exponent > 1)) or\
						((a_prime == 2) and (an_exponent > 2)):	# Computation too long!
						
						continue
					
					for a_third_constant in range(a_prime**an_exponent):
						
						if a_third_constant % a_prime == 0:	# Left as an exercise!
							
							continue
						
						if a_third_constant < a_first_constant:	# Known by symmetry.
							
							continue
						
						if a_third_constant < a_second_constant:	# Known by symmetry.
							
							continue
						
						print("\t\t\t\t\tThird doubling: {0:d}, {1:d}, {2:d}".format(a_first_constant, a_second_constant, a_third_constant))
						matchmaker.write("\t\t\t\t\tThird doubling: {0:d}, {1:d}, {2:d}\n".format(a_first_constant, a_second_constant, a_third_constant))
						
						the_third_doubling =\
							cayley_dickson.cayley_dickson_doubling(the_second_doubling, the_base_ring(a_third_constant))
						
						if a_prime == 2:
							
							predicted_third = 2**(8*an_exponent-1)
						
						else:
							
							predicted_third = a_prime**(8*an_exponent)
							predicted_third -= (a_prime**7+(a_prime-1)*a_prime**3)*a_prime**(8*(an_exponent-1))
						
						third_before = datetime.datetime.now()
						
						nb_of_invertibles_third = 0
						
						for first_component, second_component, third_component, fourth_component,\
							fifth_component, sixth_component, seventh_component, eigth_component in\
							itertools.product(range(a_prime**an_exponent), repeat = 8):
							
							an_element_in_the_third_doubling =\
								the_third_doubling(the_second_doubling(the_first_doubling(the_base_ring(first_component), the_base_ring(second_component)),
								the_first_doubling(the_base_ring(third_component), the_base_ring(fourth_component))),
								the_second_doubling(the_first_doubling(the_base_ring(fifth_component), the_base_ring(sixth_component)),
								the_first_doubling(the_base_ring(seventh_component), the_base_ring(eigth_component))))
							
							if an_element_in_the_third_doubling.is_invertible():
								
								nb_of_invertibles_third += 1
						
						third_after = datetime.datetime.now()
						
						print("\t\t\t\t\t\tfound {0:d} - expected {1:d} - ".format(nb_of_invertibles_third, predicted_third)+
							("Match!" if (nb_of_invertibles_third == predicted_third) else "Hello Houston…"))
						matchmaker.write("\t\t\t\t\t\tfound {0:d} - expected {1:d} - ".format(nb_of_invertibles_third, predicted_third)+
							("Match!" if (nb_of_invertibles_third == predicted_third) else "Hello Houston…"))
						print("\t\t\t\t\t\tcomputation time: ", third_after-third_before)
						
						print("\n")
						matchmaker.write("\n")
			
			print("\n")
			matchmaker.write("\n")


# In the end, there can be None

sys.exit(0)

