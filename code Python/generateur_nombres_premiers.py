"""
generateur_nombres_premiers
"""
import os
import random
import secrets

import euclide_etendu

import crypto_config


def charge_liste_premiers(min, max, exposant_publique):
	
	#C.f. https://stackoverflow.com/questions/5137497/find-current-directory-and-files-directory
	
	ce_chemin = os.path.dirname(os.path.realpath(__file__))
	
	liste_premiers = []
	
	for fichier in ["primes1.txt", "primes2.txt", "primes3.txt"]:
		
		chemin_fichier_premiers = os.path.join(ce_chemin, "Premiers", fichier)
		
		if not os.path.exists(chemin_fichier_premiers):
			
			raise RuntimeError("Fichier "+fichier+" non trouvé!")
		
		with open(chemin_fichier_premiers, "r") as fp:
			
			saut = False
			
			for line in iter(fp.readline, ''):
				
				if not saut:
					
					saut = True
					
					continue
				
				for chaine_premier in line.split():
					
					nb_premier = int(chaine_premier)
					
					if nb_premier >= min and nb_premier%exposant_publique !=1:
						
						liste_premiers.append(nb_premier)
					
					if nb_premier >= max:
						
						return liste_premiers
	
	return liste_premiers

min =0
max =11223
exposant_publique =3

# print(charge_liste_premiers(min, max, exposant_publique))

def generateur_cle_privee(exposant_publique, taille_paquet):	#Finir
	"""
	Renvoie un couple de nombre premier constituant la clès privée.
	Attention, il faut que exposant_publique soit premier (ceci n'est pas testé)!
	"""
	
	# TODO: choisir aléatoirement deux nombre premiers *distincts* de bonne_liste
	# le produit des nombres doit être < 2^32 et par exemple chaque nombre >= 2^10
	
	bonne_liste = charge_liste_premiers((1<<(2*taille_paquet)),(1<<(7*taille_paquet)) ,exposant_publique )
	#bonne_liste = [nb for nb in liste_premiers if (nb-1) % exposant_publique != 0]
	
	longueur = len(bonne_liste)
	
	if crypto_config.reproductibilité:
		
		premier_tirage = random.randrange(longueur)
		premier_nombre = bonne_liste[premier_tirage]
		
		#Refair une liste en supprimant premier_tirage,
		#puis en cherchant la borne du slice
		#Amélioration possible: recherche dichotomique plutôt qu'exaustive.
		del bonne_liste[premier_tirage]
		
		taille_min = (1<<(8*taille_paquet))//premier_nombre
		
		indice_bas = 0
		
		while (bonne_liste[indice_bas] < taille_min and indice_bas < len(bonne_liste)-1):
			
			indice_bas +=1
		
		taile_max = (1<<(9*taille_paquet))//premier_nombre
		
		indice_haut = indice_bas-1
		
		while (bonne_liste[indice_haut] < taile_max and indice_haut < len(bonne_liste)-1):
			
			indice_haut +=1
		
		nouvelle_liste = bonne_liste[indice_bas : indice_haut]
		
		if len(nouvelle_liste) == 0:
			
			return generateur_cle_privee(exposant_publique, taille_paquet)
		
		second_nombre = random.choice(nouvelle_liste)
		
		return (premier_nombre, second_nombre)
	
	else:
		
		premier_tirage = secrets.randbelow(longueur-1)
		#print(premier_tirage)
		premier_nombre = bonne_liste[premier_tirage]
		
		del bonne_liste[premier_tirage]
		
		#taille_min = (1<<(8*taille_paquet))//premier_nombre
		
		indice_bas = 0
		
		#while (bonne_liste[indice_bas] < taille_min and indice_bas < len(bonne_liste)-1):
		while (premier_nombre*bonne_liste[indice_bas] < (1<<(8*taille_paquet)) and indice_bas < len(bonne_liste)-1):
			
			indice_bas +=1
		
		#taile_max = (1<<(9*taille_paquet))//premier_nombre
		
		indice_haut = indice_bas-1
		
		#while (bonne_liste[indice_haut] < taile_max and indice_haut < len(bonne_liste)-1):
		while (premier_nombre*bonne_liste[indice_haut] < (1<<(9*taille_paquet)) and indice_haut < len(bonne_liste)-1):
			
			indice_haut +=1
		
		nouvelle_liste = bonne_liste[indice_bas : indice_haut]
		
		if len(nouvelle_liste) == 0:
			
			return generateur_cle_privee(exposant_publique, taille_paquet)
		
		second_nombre = secrets.choice(nouvelle_liste)	# TODO: s'assurer qu'il y en ait un
		
		return (premier_nombre, second_nombre)



taille_paquet = 2
print(generateur_cle_privee(exposant_publique, taille_paquet))


















































