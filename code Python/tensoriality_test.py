#!/usr/bin/env python3
"""
tensoriality_test

Invoque with: python3 path_to_this_script


Positional (mandatory) argument:

Keyword (optional) arguments:


This script tests the tensoriality of objects created via the Cayley-Dickson doubling
procedure.
"""


import os
import sys
import argparse
import codecs
import datetime
import itertools


import arithmetic
import congruence_rings
import cayley_dickson


# Constants definitions

inch = 2.54					# in cm

minutes_in_a_day = 1440

absolute_zero = -273.15		# in °C

outputs_top_storage_name = "Outputs"

fig_extension = "pdf"

a_few_tests = {
	2:	[1, 2, 3],
	3:	[1, 2],
	5:	[1, 2],
	7:	[1, 2],
	11:	[1],
	13:	[1]}

# Utility objects and functions

def predicted_base_invertibles_computation(a_prime, an_exponent):
	
	if a_prime == 2:
		
		predicted_base = 2**(an_exponent-1)
	
	else:
		
		predicted_base = (a_prime-1)*(a_prime**(an_exponent-1))
	
	return predicted_base


def predicted_base_unimodulars_computation(a_prime, an_exponent):
	
	if a_prime == 2:
		
		if an_exponent == 1:
			
			predicted_base = 1
		
		elif an_exponent == 2:
			
			predicted_base = 2
		
		else:
			
			predicted_base = 4
	
	else:
		
		predicted_base = 2
	
	return predicted_base


def predicted_first_invertibles_computation(a_prime, an_exponent):
	
	if a_prime == 2:
		
		predicted_first = 2**(2*an_exponent-1)
	
	else:
		
		factor_first = arithmetic.Jacobi(a_first_constant % a_prime, a_prime)
		factor_first *= (1 if (((a_prime-1)>>1) % 2 == 0) else -1)
		
		predicted_first = a_prime**(2*an_exponent)
		predicted_first -= (a_prime+(a_prime-1)*factor_first)*a_prime**(2*(an_exponent-1))
	
	return predicted_first


def predicted_first_unimodulars_computation(a_prime, an_exponent):
	
	if a_prime == 2:
		
		if an_exponent == 1:
			
			predicted_first = 2
		
		else:
			
			differentiator = (a_first_constant>>1)-((a_first_constant>>2)<<1)
			predicted_first = 2**(an_exponent+1-differentiator)
	
	else:
		
		factor_first = arithmetic.Jacobi(a_first_constant % a_prime, a_prime)
		factor_first *= (1 if (((a_prime-1)>>1) % 2 == 0) else -1)
		
		predicted_first = a_prime**(an_exponent-1)
		predicted_first *= a_prime-factor_first
	
	return predicted_first


def predicted_second_invertibles_computation(a_prime, an_exponent):
	
	if a_prime == 2:
		
		predicted_second = 2**(4*an_exponent-1)
	
	else:
		
		predicted_second = a_prime**(4*an_exponent)
		predicted_second -= (a_prime**3+(a_prime-1)*a_prime)*a_prime**(4*(an_exponent-1))
	
	return predicted_second


def predicted_second_unimodulars_computation(a_prime, an_exponent):
	
	if a_prime == 2:
		
		predicted_second = 2**(3*an_exponent)
	
	else:
		
		predicted_second = a_prime**(3*(an_exponent-1))
		predicted_second *= a_prime**3-a_prime
	
	return predicted_second


def predicted_third_invertibles_computation(a_prime, an_exponent):
	
	if a_prime == 2:
		
		predicted_third = 2**(8*an_exponent-1)
	
	else:
		
		predicted_third = a_prime**(8*an_exponent)
		predicted_third -= (a_prime**7+(a_prime-1)*a_prime**3)*a_prime**(8*(an_exponent-1))
	
	return predicted_third


def predicted_third_unimodulars_computation(a_prime, an_exponent):
	
	if a_prime == 2:
		
		predicted_third = 2**(7*an_exponent)
	
	else:
		
		predicted_third = a_prime**(7*(an_exponent-1))
		predicted_third *= a_prime**7-a_prime**3
	
	return predicted_third


# CLI exploitation

common_path = os.path.dirname(sys.argv[0])

this_script_name = os.path.splitext(os.path.basename(sys.argv[0]))[0]

parser = argparse.ArgumentParser(description = "This script the tensoriality of objects "+
	"created via the Cayley-Dickson doubling procedure.")

args = parser.parse_args()


# Create output folders

top_outputs_path = os.path.join(common_path, outputs_top_storage_name)

if not os.path.exists(top_outputs_path):
	os.mkdir(top_outputs_path)

outputs_path = os.path.join(top_outputs_path, this_script_name)

if not os.path.exists(outputs_path):
	os.mkdir(outputs_path)


# Schmurf

with codecs.open(os.path.join(outputs_path, "matchmaker"+".txt"), 'w', "utf-8-sig")\
	as matchmaker:
	
	for a_first_prime, a_second_prime in itertools.product(a_few_tests, a_few_tests):
		
		if a_second_prime <= a_first_prime:	# Known by symmetry.
			
			continue
		
		for a_first_exponent, a_second_exponent in\
			itertools.product(a_few_tests[a_first_prime], a_few_tests[a_second_prime]):
			
			print("({0:d}, {1:d}) ({2:d}, {3:d})".format(a_first_prime, a_first_exponent,
				a_second_prime, a_second_exponent))
			matchmaker.write("({0:d}, {1:d}) ({2:d}, {3:d})".format(a_first_prime, a_first_exponent,
				a_second_prime, a_second_exponent))
			
			print("\n")
			matchmaker.write("\n")
			
			# Test for the base ring
			
			print("\t\tBase ring:")
			matchmaker.write("\t\tBase ring:\n")
			
			base_cardinal = a_first_prime**a_first_exponent
			base_cardinal *= a_second_prime**a_second_exponent
			
			the_base_ring = congruence_rings.congruence_ring(base_cardinal)
			
			predicted_base_invertibles =\
				predicted_base_invertibles_computation(a_first_prime, a_first_exponent)
			predicted_base_invertibles *=\
				predicted_base_invertibles_computation(a_second_prime, a_second_exponent)
			
			predicted_base_unimodulars =\
				predicted_base_unimodulars_computation(a_first_prime, a_first_exponent)
			predicted_base_unimodulars *=\
				predicted_base_unimodulars_computation(a_second_prime, a_second_exponent)
			
			base_before = datetime.datetime.now()
			
			nb_of_invertibles_base = 0
			nb_of_unimodulars_base = 0
			
			for an_integer in range(base_cardinal):
				
				an_element_in_the_base_ring = the_base_ring(an_integer)
				
				if an_element_in_the_base_ring.is_invertible():
					
					nb_of_invertibles_base += 1
				
				if an_element_in_the_base_ring.is_unimodular():
					
					nb_of_unimodulars_base += 1
			
			base_after = datetime.datetime.now()
			
			print("\t\t\tInvertibles: found {0:d} - expected {1:d} - ".format(nb_of_invertibles_base, predicted_base_invertibles)+
				("Match!" if (nb_of_invertibles_base == predicted_base_invertibles) else "Hello Houston…"))
			matchmaker.write("\t\t\tInvertibles: found {0:d} - expected {1:d} - ".format(nb_of_invertibles_base, predicted_base_invertibles)+
				("Match!" if (nb_of_invertibles_base == predicted_base_invertibles) else "Hello Houston…"))
			print("\t\t\tUnimodulars: found {0:d} - expected {1:d} - ".format(nb_of_unimodulars_base, predicted_base_unimodulars)+
				("Match!" if (nb_of_unimodulars_base == predicted_base_unimodulars) else "Hello Houston…"))
			matchmaker.write("\t\t\tUnimodulars: found {0:d} - expected {1:d} - ".format(nb_of_unimodulars_base, predicted_base_unimodulars)+
				("Match!" if (nb_of_unimodulars_base == predicted_base_unimodulars) else "Hello Houston…"))
			print("\t\t\tcomputation time: ", base_after-base_before)
			
			print("\n")
			matchmaker.write("\n")
			
			# Test for the first doubling
			
			for a_first_constant in range(base_cardinal):
				
				if (a_first_constant % a_first_prime == 0) or (a_first_constant % a_second_prime == 0):	# Left as an exercise!
					
					continue
				
				print("\t\t\tFirst doubling: {0:d}".format(a_first_constant))
				matchmaker.write("\t\t\tFirst doubling: {0:d}\n".format(a_first_constant))
				
				the_first_doubling =\
					cayley_dickson.cayley_dickson_doubling(the_base_ring, the_base_ring(a_first_constant))
				
				predicted_first_invertibles =\
					predicted_first_invertibles_computation(a_first_prime, a_first_exponent)
				predicted_first_invertibles *=\
					predicted_first_invertibles_computation(a_second_prime, a_second_exponent)
				
				predicted_first_unimodulars =\
					predicted_first_unimodulars_computation(a_first_prime, a_first_exponent)
				predicted_first_unimodulars *=\
					predicted_first_unimodulars_computation(a_second_prime, a_second_exponent)
				
				first_before = datetime.datetime.now()
								
				nb_of_invertibles_first = 0
				nb_of_unimodulars_first = 0
				
				for first_component, second_component in\
					itertools.product(range(base_cardinal), repeat = 2):
					
					an_element_in_the_first_doubling =\
						the_first_doubling(the_base_ring(first_component), the_base_ring(second_component))
					
					if an_element_in_the_first_doubling.is_invertible():
						
						nb_of_invertibles_first += 1
					
					if an_element_in_the_first_doubling.is_unimodular():
						
						nb_of_unimodulars_first += 1
				
				first_after = datetime.datetime.now()
				
				print("\t\t\t\tInvertibles: found {0:d} - expected {1:d} - ".format(nb_of_invertibles_first, predicted_first_invertibles)+
					("Match!" if (nb_of_invertibles_first == predicted_first_invertibles) else "Hello Houston…"))
				matchmaker.write("\t\t\t\tInvertibles: found {0:d} - expected {1:d} - ".format(nb_of_invertibles_first, predicted_first_invertibles)+
					("Match!" if (nb_of_invertibles_first == predicted_first_invertibles) else "Hello Houston…"))
				print("\t\t\t\tUnimodulars: found {0:d} - expected {1:d} - ".format(nb_of_unimodulars_first, predicted_first_unimodulars)+
					("Match!" if (nb_of_unimodulars_first == predicted_first_unimodulars) else "Hello Houston…"))
				matchmaker.write("\t\t\t\tUnimodulars: found {0:d} - expected {1:d} - ".format(nb_of_unimodulars_first, predicted_first_unimodulars)+
					("Match!" if (nb_of_unimodulars_first == predicted_first_unimodulars) else "Hello Houston…"))
				print("\t\t\t\tcomputation time: ", first_after-first_before)
				
				print("\n")
				matchmaker.write("\n")
				
				# Test for the second doubling
				
				for a_second_constant in range(base_cardinal):
					
					if (a_second_constant % a_first_prime == 0) or (a_second_constant % a_second_prime == 0):	# Left as an exercise!
						
						continue
					
					if a_second_constant < a_first_constant:	# Known by symmetry.
						
						continue
					
					print("\t\t\t\tSecond doubling: {0:d}, {1:d}".format(a_first_constant, a_second_constant))
					matchmaker.write("\t\t\t\tSecond doubling: {0:d}, {1:d}\n".format(a_first_constant, a_second_constant))
					
					the_second_doubling =\
						cayley_dickson.cayley_dickson_doubling(the_first_doubling, the_base_ring(a_second_constant))
					
					predicted_second_invertibles =\
						predicted_second_invertibles_computation(a_first_prime, a_first_exponent)
					predicted_second_invertibles *=\
						predicted_second_invertibles_computation(a_second_prime, a_second_exponent)
					
					predicted_second_unimodulars =\
						predicted_second_unimodulars_computation(a_first_prime, a_first_exponent)
					predicted_second_unimodulars *=\
						predicted_second_unimodulars_computation(a_second_prime, a_second_exponent)
					
					second_before = datetime.datetime.now()
					
					nb_of_invertibles_second = 0
					nb_of_unimodulars_second = 0
					
					for first_component, second_component, third_component, fourth_component in\
						itertools.product(range(base_cardinal), repeat = 4):
						
						an_element_in_the_second_doubling =\
							the_second_doubling(the_first_doubling(the_base_ring(first_component), the_base_ring(second_component)),
							the_first_doubling(the_base_ring(third_component), the_base_ring(fourth_component)))
						
						if an_element_in_the_second_doubling.is_invertible():
							
							nb_of_invertibles_second += 1
						
						if an_element_in_the_second_doubling.is_unimodular():
							
							nb_of_unimodulars_second += 1
					
					second_after = datetime.datetime.now()
					
					print("\t\t\t\t\tInvertibles: found {0:d} - expected {1:d} - ".format(nb_of_invertibles_second, predicted_second_invertibles)+
						("Match!" if (nb_of_invertibles_second == predicted_second_invertibles) else "Hello Houston…"))
					matchmaker.write("\t\t\t\t\tInvertibles: found {0:d} - expected {1:d} - ".format(nb_of_invertibles_second, predicted_second_invertibles)+
						("Match!" if (nb_of_invertibles_second == predicted_second_invertibles) else "Hello Houston…"))
					print("\t\t\t\t\tUnimodulars: found {0:d} - expected {1:d} - ".format(nb_of_unimodulars_second, predicted_second_unimodulars)+
						("Match!" if (nb_of_unimodulars_second == predicted_second_unimodulars) else "Hello Houston…"))
					matchmaker.write("\t\t\t\t\tUnimodulars: found {0:d} - expected {1:d} - ".format(nb_of_unimodulars_second, predicted_second_unimodulars)+
						("Match!" if (nb_of_unimodulars_second == predicted_second_unimodulars) else "Hello Houston…"))
					print("\t\t\t\t\tcomputation time: ", second_after-second_before)
					
					print("\n")
					matchmaker.write("\n")
					
					# Test for the third doubling
					
					if ((a_first_prime != 2) and (a_first_exponent > 1)) or\
						((a_second_prime != 2) and (a_second_exponent > 1)) or\
						((a_first_prime == 2) and (a_first_exponent > 2)):	# Computation too long!
						
						continue
					
					for a_third_constant in range(base_cardinal):
						
						if (a_third_constant % a_first_prime == 0) or (a_third_constant % a_second_prime == 0):	# Left as an exercise!
							
							continue
						
						if a_third_constant < a_first_constant:	# Known by symmetry.
							
							continue
						
						if a_third_constant < a_second_constant:	# Known by symmetry.
							
							continue
						
						print("\t\t\t\t\tThird doubling: {0:d}, {1:d}, {2:d}".format(a_first_constant, a_second_constant, a_third_constant))
						matchmaker.write("\t\t\t\t\tThird doubling: {0:d}, {1:d}, {2:d}\n".format(a_first_constant, a_second_constant, a_third_constant))
						
						the_third_doubling =\
							cayley_dickson.cayley_dickson_doubling(the_second_doubling, the_base_ring(a_third_constant))
						
						predicted_third_invertibles =\
							predicted_third_invertibles_computation(a_first_prime, a_first_exponent)
						predicted_third_invertibles *=\
							predicted_third_invertibles_computation(a_second_prime, a_second_exponent)
						
						predicted_third_unimodulars =\
							predicted_third_unimodulars_computation(a_first_prime, a_first_exponent)
						predicted_third_unimodulars *=\
							predicted_third_unimodulars_computation(a_second_prime, a_second_exponent)
						
						third_before = datetime.datetime.now()
						
						nb_of_invertibles_third = 0
						nb_of_unimodulars_third = 0
						
						for first_component, second_component, third_component, fourth_component,\
							fifth_component, sixth_component, seventh_component, eigth_component in\
							itertools.product(range(base_cardinal), repeat = 8):
							
							an_element_in_the_third_doubling =\
								the_third_doubling(the_second_doubling(the_first_doubling(the_base_ring(first_component), the_base_ring(second_component)),
								the_first_doubling(the_base_ring(third_component), the_base_ring(fourth_component))),
								the_second_doubling(the_first_doubling(the_base_ring(fifth_component), the_base_ring(sixth_component)),
								the_first_doubling(the_base_ring(seventh_component), the_base_ring(eigth_component))))
							
							if an_element_in_the_third_doubling.is_invertible():
								
								nb_of_invertibles_third += 1
							
							if an_element_in_the_third_doubling.is_unimodular():
								
								nb_of_unimodulars_third += 1
						
						third_after = datetime.datetime.now()
						
						print("\t\t\t\t\t\tInvertibles: found {0:d} - expected {1:d} - ".format(nb_of_invertibles_third, predicted_third_invertibles)+
							("Match!" if (nb_of_invertibles_third == predicted_third_invertibles) else "Hello Houston…"))
						matchmaker.write("\t\t\t\t\t\tInvertibles: found {0:d} - expected {1:d} - ".format(nb_of_invertibles_third, predicted_third_invertibles)+
							("Match!" if (nb_of_invertibles_third == predicted_third_invertibles) else "Hello Houston…"))
						print("\t\t\t\t\t\tUnimodulars: found {0:d} - expected {1:d} - ".format(nb_of_unimodulars_third, predicted_third_unimodulars)+
							("Match!" if (nb_of_unimodulars_third == predicted_third_unimodulars) else "Hello Houston…"))
						matchmaker.write("\t\t\t\t\t\tUnimodulars: found {0:d} - expected {1:d} - ".format(nb_of_unimodulars_third, predicted_third_unimodulars)+
							("Match!" if (nb_of_unimodulars_third == predicted_third_unimodulars) else "Hello Houston…"))
						print("\t\t\t\t\t\tcomputation time: ", third_after-third_before)
						
						print("\n")
						matchmaker.write("\n")
			
			print("\n")
			matchmaker.write("\n")


# In the end, there can be None

sys.exit(0)

