#!/usr/bin/env python3
"""
congruence_rings_test

Invoque with: python3 path_to_this_script


Positional (mandatory) argument:

Keyword (optional) arguments:


This script tests congrurence ring objects.
"""


import os
import sys
import argparse
import codecs


import congruence_rings


# Constants definitions

inch = 2.54					# in cm

minutes_in_a_day = 1440

absolute_zero = -273.15		# in °C

outputs_top_storage_name = "Outputs"

fig_extension = "pdf"


# Utility objects and functions


# CLI exploitation

common_path = os.path.dirname(sys.argv[0])

this_script_name = os.path.splitext(os.path.basename(sys.argv[0]))[0]

parser = argparse.ArgumentParser(description = "This script tests congruence ring objects.")

args = parser.parse_args()


# Check output storage, and perhaps create it

outputs_path = os.path.join(common_path, outputs_top_storage_name)

if not os.path.exists(outputs_path):
	os.mkdir(outputs_path)


# Schmurf

try:
	
	Z_1 = congruence_rings.congruence_ring(1)

except ValueError as err:
	
	print("That's not what we want! {0:s}".format(str(err.args[0])))

Z_3 = congruence_rings.congruence_ring(3)

print("The base ring of Z_3 is {0:s}".format(str(Z_3.base_ring)))
print("The current algebra of Z_3 is {0:s}".format(str(Z_3.current_algebra)))

default_value_in_Z_3 = Z_3()

print("The default value of an element in Z_3 is {0:s}".format(default_value_in_Z_3))

neutral_element_for_multiplication_of_Z_3 = Z_3.neutral_element_for_multiplication()

print("the neutral element for the multiplication of Z_3 is "+
	"{0:s}".format(neutral_element_for_multiplication_of_Z_3))

an_upcasted_element = Z_3.upcast(2)

print("Z_3.upcast(2) = {0:s}".format(an_upcasted_element))

class_of_7_in_Z_3 = Z_3(7)

print("The class of 7 in Z_3 is {0:s}".format(class_of_7_in_Z_3))

print("class_of_7_in_Z_3.dump() = {0}".format(class_of_7_in_Z_3.dump()))

print("class_of_7_in_Z_3.flatten() = {0}".format(class_of_7_in_Z_3.flatten()))

class_of_2_in_Z_3 = Z_3(2)

print("The class of 2 in Z_3 is {0:s}".format(class_of_2_in_Z_3))

class_of_2_in_Z_3_equals_class_of_2_in_Z_3 = (class_of_2_in_Z_3 == class_of_2_in_Z_3)

print("class_of_2_in_Z_3 == class_of_2_in_Z_3 is "+
	"{0}".format(class_of_2_in_Z_3_equals_class_of_2_in_Z_3))

class_of_7_in_Z_3_not_equals_class_of_2_in_Z_3 = (class_of_7_in_Z_3 != class_of_2_in_Z_3)

print("class_of_7_in_Z_3 != class_of_2_in_Z_3 is "+
	"{0}".format(class_of_7_in_Z_3_not_equals_class_of_2_in_Z_3))

opposite_of_class_of_2_in_Z_3 = -class_of_2_in_Z_3

print("-class_of_2_in_Z_3 = {0:s}".format(opposite_of_class_of_2_in_Z_3))

an_addition_in_Z_3 = class_of_7_in_Z_3+class_of_2_in_Z_3

print("class_of_7_in_Z_3+class_of_2_in_Z_3 = {0:s}".format(an_addition_in_Z_3))

a_substraction_in_Z_3 = class_of_7_in_Z_3-class_of_2_in_Z_3

print("class_of_7_in_Z_3-class_of_2_in_Z_3 = {0:s}".format(a_substraction_in_Z_3))

a_multiplication_in_Z_3 = class_of_7_in_Z_3*class_of_2_in_Z_3

print("class_of_7_in_Z_3*class_of_2_in_Z_3 = {0:s}".format(a_multiplication_in_Z_3))

class_of_2_in_Z_3_is_invertible = class_of_2_in_Z_3.is_invertible()

print("The class of 2 in Z_3 is invertible: {0}".format(class_of_2_in_Z_3_is_invertible))

inverse_of_class_of_2_in_Z_3 = class_of_2_in_Z_3.inverse()

print("The inverse of the class of 2 in Z_3 is {0:s}".format(inverse_of_class_of_2_in_Z_3))

Z_6 = congruence_rings.congruence_ring(6)

class_of_2_in_Z_6 = Z_6(2)

print("class_of_7_in_Z_3 is an element of Z_3: {0}".format(isinstance(class_of_7_in_Z_3, Z_3)))

print("class_of_7_in_Z_3 is an element of Z_6: {0}".format(isinstance(class_of_7_in_Z_3, Z_6)))

class_of_2_in_Z_6_is_invertible = class_of_2_in_Z_6.is_invertible()

print("The class of 2 in Z_6 is invertible: {0}".format(class_of_2_in_Z_6_is_invertible))

try:
	
	class_of_2_in_Z_6.inverse()

except ZeroDivisionError as err:
	
	print("That's not possible! {0:s}".format(str(err.args[0])))

a_division_in_Z_3 = class_of_7_in_Z_3/class_of_2_in_Z_3

print("class_of_7_in_Z_3/class_of_2_in_Z_3 = {0:s}".format(a_division_in_Z_3))

an_unimodular_of_Z_6 = Z_6(1)

a_not_unimodular_of_Z_6 = Z_6(3)

print("{0:s} is unimodular: {1}".format(an_unimodular_of_Z_6, an_unimodular_of_Z_6.is_unimodular()))

print("{0:s} is unimodular: {1}".format(a_not_unimodular_of_Z_6, a_not_unimodular_of_Z_6.is_unimodular()))

an_exponentiation_in_Z_3 = class_of_2_in_Z_3**3

print("class_of_2_in_Z_3**3 = {0:s}".format(an_exponentiation_in_Z_3))

a_conjugation_in_Z_3 = class_of_7_in_Z_3.conjugate()

print("class_of_7_in_Z_3.conjugate() = {0:s}".format(a_conjugation_in_Z_3))

equality_of_the_conjugate = (class_of_7_in_Z_3 == a_conjugation_in_Z_3)

print("class_of_7_in_Z_3 == class_of_7_in_Z_3.conjugate() is {0}".format(equality_of_the_conjugate))

object_is_not_its_class = (class_of_7_in_Z_3 == 1)

print("class_of_7_in_Z_3 == 1 is {0}".format(object_is_not_its_class))

try:
	
	Z_7 = congruence_rings.congruence_ring(7)
	
	class_of_3_in_Z_7 = Z_7(3)
	
	a_meaningless_addition = class_of_7_in_Z_3+class_of_3_in_Z_7

except TypeError as err:
	
	print("That's nonsense! {0:s}".format(str(err.args[0])))


# In the end, there can be None

sys.exit(0)


