#!/usr/bin/env python3
"""
coeur_RSA_octonionique

Ce fichier imlémente le cœur de l'encryption et décryption de la 
variante octonionique de l'algorithme RSA.

"""

import os


import euclide_etendu

#import crypto_config

import congruence_rings
import cayley_dickson
import octonions
import coeur_RSA
import random
import secrets


#from crypto_config import reproductibilité, RSA_config

def charge_liste_premiers_octo(min, max, exposant_publique): #Copie à phy(n) près de celle de coeur_RSA
	
	#C.f. https://stackoverflow.com/questions/5137497/find-current-directory-and-files-directory
	
	ce_chemin = os.path.dirname(os.path.realpath(__file__))
	
	liste_premiers = []
	
	for fichier in ["primes1.txt", "primes2.txt", "primes3.txt"]:
		
		chemin_fichier_premiers = os.path.join(ce_chemin, "Premiers", fichier)
		
		if not os.path.exists(chemin_fichier_premiers):
			
			raise RuntimeError("Fichier "+fichier+" non trouvé!")
		
		with open(chemin_fichier_premiers, "r") as fp:
			
			saut = False
			
			for line in iter(fp.readline, ''):
				
				if not saut:
					
					saut = True
					
					continue
				
				for chaine_premier in line.split():
					
					nb_premier = int(chaine_premier)
					
					
					if nb_premier >= min and euclide_etendu.euclide_etendu(exposant_publique , indicatrice_premier_octo(nb_premier))[2] ==1:
						
						liste_premiers.append(nb_premier)
						#le_testeur = euclide_etendu.euclide_etendu( nb_premier, indicatrice_premier_octo(exposant_publique))
						#print(le_testeur)
					
					if nb_premier >= max:
						
						return liste_premiers
	
	return liste_premiers
#faire tout le machin pour les nombres premiers

def encode_RSA_octo( paquet_info, structures, reproductible = False):
	"""
	encode une séquence d'octets en un octonion inversible dont la première
	composante est choisi aléatoire;
	attention la longueur de la liste doit être un multiple de 7
	"""
	
	longueur = len(paquet_info)
	
	if longueur % 7 != 0: #probablement à supprimer, peu efficace
		raise RuntimeError ("Sequence de longueur non divisible par 7")
	
	buffer = [0]
	
	taille_composantes = longueur//7
	
	for i in range(7):
	
		composante = 0
		
		for j in range(i*taille_composantes, (i+1)*taille_composantes):
			composante = composante << 8
			composante += paquet_info[j]
		
		buffer.append(composante)
	
	brouillon = octonions.octonion_tuple(structures, buffer)
	
	norme = brouillon.cayley_norm().downcast()
	
	if reproductible:
		
		can_proceed = False
		taille_max = 1 <<(8*longueur)
		
		while not can_proceed:
			
			premiere_composante = structures[0](random.randrange(taille_max))
			
			norme_tot = norme+(premiere_composante)**2 #par proprieté de la norme
										 # pour la 1er composante
			
			can_proceed = norme_tot.is_invertible()
		
	else:
		
		can_proceed = False
		taille_max = 1 <<(8*longueur) -1 #car randbelow peut aller jusqu'à n
		
		while not can_proceed:
			
			premiere_composante = structures[0](secrets.randbelow(taille_max))
			norme_tot = norme+(premiere_composante)**2 #par proprieté de la nomre
										 # pour la 1er composante
			
			can_proceed = norme_tot.is_invertible()
	
	return brouillon+structures[3].upcast(premiere_composante)



def decode_RSA_octo (octonion, taille_composante):
	
	res = []
	
	crepe = octonion.flatten()
	
	for i in range(7 , 0, -1): #on supprime la 1er composante (aléatoire)
		
		composante = crepe[i]
		
		for j in range(taille_composante):
			
			res.append(composante & 0xFF)	#cf coeur_RSA
			composante >>= 8
	
	return bytes(reversed(res)) 


def encode_RSA_octo_inversible( paquet_info, structures):
	"""
	encode une séquence d'octets en un octonion dans le cas où l'on sait que
	celui-ci seras inversible
	attention la longueur de la liste doit être un multiple de 8
	"""
	longueur = len(paquet_info)
	if longueur % 8 != 0:
		raise RuntimeError ("Sequence de longueur non divisible par 8")
	res = []
	taille_composantes= len(paquet_info)//8 
	for i in range (8):
		composante = 0
		for j in range(i*taille_composantes, (i+1)*taille_composantes):
			composante = composante << 8
			composante += paquet_info[j]
		res.append(composante)
	return octonions.octonion_tuple(structures, res)


def decode_RSA_octo_inversible( octonion, taille_composante):
	res = []
	crepe = octonion.flatten()
	for i in range(7 , -1, -1):
		composante = crepe[i]
		for j in range(taille_composante):
			res.append(composante & 0xFF)	#cf coeur_RSA
			composante >>= 8
	return bytes(reversed(res)) 


def encrypte_RSA_octonion (message, exposant_publique):	#pas besoin du grand nombre: il est compris dans la structure algébrique
	
	return message**exposant_publique


def decrypte_RSA_octonion (message, exposant_privee): #id  encrypte_RSA_octonion
	return message**exposant_privee

#################
def generateur_cle_privee_octonion (exposant_publique, taille_paquet, constante_1 = 1, constante_2 = 1, constante_3 = 1, reproductible = False): #TODO renommer taille_paquet en taille_composante
	"""
	TODO
	renvoie un tuple contenant 2 nombres premiers convenables
	et 3 constantes de structures,constituant la clès privée
	"""
	#return (4993,4999,constante_1,constante_2,constante_3) # premier non convenable, mais donne un resultat interressant: avec 1,1,1 comme constantes et 3 comme exposant publique, le message crypté est le même que le message non crypté
	#return  (4999,5003,constante_1,constante_2,constante_3) #TODO
	#copie à charge_liste_premiers_octo près de generateur_cle_privee de coeur_RSA
	
	# TODO: choisir aléatoirement deux nombre premiers *distincts* de bonne_liste
	# le produit des nombres doit être < 2^32 et par exemple chaque nombre >= 2^10
	
	bonne_liste = charge_liste_premiers_octo((1<<(2*taille_paquet)),(1<<(7*taille_paquet)) ,exposant_publique )
	#bonne_liste = [nb for nb in liste_premiers if (nb-1) % exposant_publique != 0]
	
	longueur = len(bonne_liste)
	
	if reproductible:
		
		premier_tirage = random.randrange(longueur)
		premier_nombre = bonne_liste[premier_tirage]
		#print ("premier_nombre premier avec phy(exposant publique) ? ", euclide_etendu.euclide_etendu(indicatrice_premier_octo(premier_nombre),exposant_publique)[2] ==1)
		#Refair une liste en supprimant premier_tirage,
		#puis en cherchant la borne du slice
		#Amélioration possible: recherche dichotomique plutôt qu'exaustive pour les bornes hautes et basse.
		del( bonne_liste[premier_tirage])
		
		taille_min = (1<<(8*taille_paquet))//premier_nombre
		
		indice_bas = 0
		
		while (bonne_liste[indice_bas] < taille_min and indice_bas < len(bonne_liste)-1):
			
			indice_bas +=1
		
		taile_max = (1<<(9*taille_paquet))//premier_nombre
		
		indice_haut = indice_bas-1
		
		while (bonne_liste[indice_haut] < taile_max and indice_haut < len(bonne_liste)-1):
			
			indice_haut +=1
		
		nouvelle_liste = bonne_liste[indice_bas : indice_haut]
		
		if len(nouvelle_liste) == 0:
			
			return generateur_cle_privee(exposant_publique, taille_paquet, constante_1 = 1, constante_2 = 1, constante_3 = 1)
		
		second_nombre = random.choice(nouvelle_liste)
		
		return (premier_nombre, second_nombre, constante_1, constante_2, constante_3)
	
	else:
		
		premier_tirage = secrets.randbelow(longueur-1)
		#print(premier_tirage)
		premier_nombre = bonne_liste[premier_tirage]
		
		del (bonne_liste[premier_tirage])
		
		#taille_min = (1<<(8*taille_paquet))//premier_nombre
		
		indice_bas = 0
		
		#while (bonne_liste[indice_bas] < taille_min and indice_bas < len(bonne_liste)-1):
		while (premier_nombre*bonne_liste[indice_bas] < (1<<(8*taille_paquet)) and indice_bas < len(bonne_liste)-1):
			
			indice_bas +=1
		
		#taile_max = (1<<(9*taille_paquet))//premier_nombre
		
		indice_haut = indice_bas-1
		
		#while (bonne_liste[indice_haut] < taile_max and indice_haut < len(bonne_liste)-1):
		while (premier_nombre*bonne_liste[indice_haut] < (1<<(9*taille_paquet)) and indice_haut < len(bonne_liste)-1):
			
			indice_haut +=1
		
		nouvelle_liste = bonne_liste[indice_bas : indice_haut]
		
		if len(nouvelle_liste) == 0:
			
			return generateur_cle_privee_octonion(exposant_publique, taille_paquet, constante_1 = 1, constante_2 = 1, constante_3 = 1 )
		
		second_nombre = secrets.choice(nouvelle_liste)	# TODO: s'assurer qu'il y en ait un
		
		return (premier_nombre, second_nombre, constante_1, constante_2, constante_3)

def generateur_cle_publique_octonion(cle_privee, exposant_publique):
	"""
	Cette fonction renvoie un tuple (grand_nombre,constante_1, constante_2,constante_3, exposant_publique)
	"""
	
	grand_nombre = cle_privee[0] * cle_privee[1]
	
	return (grand_nombre,cle_privee[2],cle_privee[3],cle_privee[4],exposant_publique )


def trouve_exposant_prive_octonion(cle_privee, expo_publique):
	"""
	ssi les nombres premier sont differents de 2 et differents entre eux,
	et phy(n) premier avec l'exposant publique
	"""
	
	#print("Clef privée: ", cle_privee)	# TODO
	#print("Exposant publique: ", expo_publique)	# TODO
	
	nb_1 =  cle_privee[0]
	nb_2 =  cle_privee[1]
	
	phy_n = (nb_1**8 - (nb_1**7+(nb_1 -1)* nb_1**3))*(nb_2**8 - (nb_2**7+(nb_2 -1)* nb_2**3))
	
	#print("phi_n: ", phy_n)	# TODO
	
	paquet = euclide_etendu.euclide_etendu(expo_publique, phy_n)
	
	#print("Après Euclide: ", paquet)	# TODO
	
	exposant_prive = paquet[0]
	
	if exposant_prive < 0:
		
		exposant_prive += phy_n
	
	return exposant_prive


def indicatrice_premier_octo(n):
	"""
	nombres d'inversible de l'anneau de congruence n doublé 3 fois pour n premier
	"""
	return (n**8 - (n**7+(n -1)* n**3))


def premier_convenable( premier, exposant_publique):
	"""
	indique si le nombre premier "premier" est compatible avec l'exposant publique,
	lorsqu'il est plus grand que se dernier
	"""
	return indicatrice_premier_octo(premier) % exposant_publique != 0