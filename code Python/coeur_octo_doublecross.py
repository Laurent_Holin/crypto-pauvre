"""
coeur_octo_doublecross

Ce fichier imlémente le cœur de l'encryption et décryption de l'algorithme
à clès secret basé sur les octonions appellé
Simple non-commutative and non-associative tethered obfuscations dans l'article
et que nous renommerons octo doublecross (odc) par simplicité

"""

import os

import random
import secrets

import euclide_etendu
import congruence_rings
import cayley_dickson
import octonions
import coeur_RSA


def generateur_clé_odc(taille_composante, constante_de_structure1 =1, constante_de_structure2 =1, constante_de_structure3=1, reproductible = False):
	"""
	revoit un tuple composé  de deux octonions inversibles
	de Z/nZ où n = 2**(8*taille_composante),qui ne commutent pas entre eux,
	de taille_composante et des constantes de structure
	'reproductible' détermine si les composantes aléatoires sont déterminées 
	par des fonctions issue des bibliothèques random ou secrets
	"""
	grand_nombre = 2**(8*taille_composante)
	structures = octonions.paquet_structure_octo(grand_nombre, constante_de_structure1 =1, constante_de_structure2 =1, constante_de_structure3=1)
	brouillons_liste_1 = [0]
	
	if reproductible:
		
		for i in range(7):
			brouillons_liste_1.append(random.randrange(grand_nombre))
		
		brouillons_octo_1 = octonions.octonion_tuple(structures,brouillons_liste_1) #???
		norme1 = brouillons_octo_1.cayley_norm().downcast()
		
		drapeau1 = True
		while drapeau1:
			premiere_composante_1 = structures[0](random.randrange(grand_nombre))
			
			norme_tot_1 = norme1+(premiere_composante_1)**2 #par proprieté de la norme
										 # pour la 1er composante
			
			drapeau1 = not norme_tot_1.is_invertible()
		
		premier_octonion = brouillons_octo_1 + structures[3].upcast(premiere_composante_1)
		
		drapeau_commute = True
		compteur_essai = 0
		while drapeau_commute and compteur_essai< 50:
			brouillons_liste_2 = [0]
			
			for i in range(7):
				brouillons_liste_2.append(random.randrange(grand_nombre))
		
			brouillons_octo_2 = octonions.octonion_tuple(structures,brouillons_liste_2)
			norme2 = brouillons_octo_2.cayley_norm().downcast()
			drapeau2 = True
			while drapeau2:
				premiere_composante_2 = structures[0](random.randrange(grand_nombre))
				
				norme_tot_2 = norme2 +(premiere_composante_2)**2 #par proprieté de la norme
											 # pour la 1er composante
				
				drapeau2 = not norme_tot_2.is_invertible()
			
			deuxieme_octonion = brouillons_octo_2 + structures[3].upcast(premiere_composante_2)
			
			drapeau_commute = (premier_octonion*deuxieme_octonion == deuxieme_octonion*premier_octonion)
			compteur_essai +=1
			if compteur_essai ==50:
				return generateur_clé_odc(taille_composante, constante_de_structure1 =1, constante_de_structure2 =1, constante_de_structure3=1, reproductible = False)
		
		return (premier_octonion,deuxieme_octonion,taille_composante, constante_de_structure1, constante_de_structure2, constante_de_structure3)
	
	else:
		for i in range(7):
			brouillons_liste_1.append(secrets.randbelow(grand_nombre))
		
		brouillons_octo_1 = octonions.octonion_tuple(structures,brouillons_liste_1)
		norme1 = brouillons_octo_1.cayley_norm().downcast()
		
		drapeau1 = True
		while drapeau1:
			premiere_composante_1 = structures[0](secrets.randbelow(grand_nombre))
			
			norme_tot_1 = norme1+(premiere_composante_1)**2 #par proprieté de la norme
										 # pour la 1er composante
			
			drapeau1 = not norme_tot_1.is_invertible()
		
		premier_octonion = brouillons_octo_1 + structures[3].upcast(premiere_composante_1)
		
		drapeau_commute = True
		
		compteur_essai = 0
		while drapeau_commute and compteur_essai< 50:
			brouillons_liste_2 = [0]
			
			for i in range(7):
				brouillons_liste_2.append(secrets.randbelow(grand_nombre))
		
			brouillons_octo_2 = octonions.octonion_tuple(structures,brouillons_liste_2)
			norme2 = brouillons_octo_2.cayley_norm().downcast()
			drapeau2 = True
			
			while drapeau2:
				premiere_composante_2 = structures[0](secrets.randbelow(grand_nombre))
				
				norme_tot_2 = norme2+(premiere_composante_2)**2 #par proprieté de la norme
											 # pour la 1er composante
				drapeau2 = not norme_tot_2.is_invertible()
			
			deuxieme_octonion = brouillons_octo_2 + structures[3].upcast(premiere_composante_2)
			
			drapeau_commute = premier_octonion*deuxieme_octonion ==deuxieme_octonion*premier_octonion
			compteur_essai +=1
		if compteur_essai ==50:
				return generateur_clé_odc(taille_composante, constante_de_structure1 =1, constante_de_structure2 =1, constante_de_structure3=1, reproductible = False)
		return (premier_octonion,deuxieme_octonion,taille_composante, constante_de_structure1, constante_de_structure2, constante_de_structure3)

#là encode recalcule les structures à chaque paquet d'info; il vaudrait mieux le faire une bonne fois pour toute,
#surtout qu'il vas falloir calculer grand_nombre.

def creer_paquet_structure_ocd(clé_odc):
	"""
	comme paquet_structure_octo de octonions mais avec avec une clé ocd
	"""
	return octonions.paquet_structure_octo( 2**(8*clé_odc[2]), *clé_odc[3:])

def encode_odc(paquet_info, paquet_structure):
	"""
	encode une séquence d'octets d'une longueur divisible par 8
	en un octonion non forcément inversible. Attention la fonction ne vérifie
	pas que la longueur de la séquence soit divisible par 8
	"""
	#à peu près idem encode_RSA_octo_inversible
	res = []
	taille_composantes= len(paquet_info)//8
	for i in range (8):
		composante = 0
		for j in range(i*taille_composantes, (i+1)*taille_composantes):
			composante = composante << 8
			composante += paquet_info[j]
		res.append(composante)
	return octonions.octonion_tuple(paquet_structure, res)

def decode_odc( octonion, taille_composante):
	"""
	decode un octonion en bytes
	"""
	#à peu près idem decode_RSA_inversible
	res = []
	crepe = octonion.flatten()
	for i in range(7 , -1, -1):
		composante = crepe[i]
		for j in range(taille_composante):
			res.append(composante & 0xFF)	#cf coeur_RSA
			composante >>= 8
	return bytes(reversed(res)) 

def inverseur_clé_odc(clé_odc):
	"""
	renvoie un tuple contenant les inverses des deux octonions de la clé,
	nécessaire au désencryptage
	"""
	return (clé_odc[0].inverse(),clé_odc[1].inverse())

def encrypte_odc(message, clé_odc):
	return (clé_odc[0]*message)*clé_odc[1]

def decrypte_odc(cryptogramme,inverse_clé):
	return inverse_clé[0]*(cryptogramme*inverse_clé[1])







































