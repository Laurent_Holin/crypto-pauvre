#!/usr/bin/env python3
"""
cayley_dickson_test

Invoque with: python3 path_to_this_script


Positional (mandatory) argument:

Keyword (optional) arguments:


This script tests objects created via the Cayley-Dickson doubling procedure.
"""


import os
import sys
import argparse
import codecs


import congruence_rings
import cayley_dickson


# Constants definitions

inch = 2.54					# in cm

minutes_in_a_day = 1440

absolute_zero = -273.15		# in °C

outputs_top_storage_name = "Outputs"

fig_extension = "pdf"


# Utility objects and functions


# CLI exploitation

common_path = os.path.dirname(sys.argv[0])

this_script_name = os.path.splitext(os.path.basename(sys.argv[0]))[0]

parser = argparse.ArgumentParser(description = "This script tests objects "+
	"created via the Cayley-Dickson doubling procedure.")

args = parser.parse_args()


# Check output storage, and perhaps create it

outputs_path = os.path.join(common_path, outputs_top_storage_name)

if not os.path.exists(outputs_path):
	os.mkdir(outputs_path)


# Schmurf

Z_5 = congruence_rings.congruence_ring(5)

class_of_2_in_Z_5 = Z_5(2)

Z_5_d_2 = cayley_dickson.cayley_dickson_doubling(Z_5, class_of_2_in_Z_5)

print("The base ring of Z_5_d_2 is {0:s}".format(str(Z_5_d_2.base_ring)))
print("The current algebra of Z_5_d_2 is {0:s}".format(str(Z_5_d_2.current_algebra)))

default_value_in_Z_5_d_2 = Z_5_d_2()

print("The default value of an element in Z_5_d_2 is {0:s}".format(default_value_in_Z_5_d_2))

base_algebra_as_element_of_the_doubling = Z_5_d_2(Z_5(3))

an_unimodular_of_Z_5_d_2 = Z_5_d_2(Z_5(1))

a_not_unimodular_of_Z_5_d_2 = Z_5_d_2(Z_5(3))

print("{0:s} is unimodular: {1}".format(an_unimodular_of_Z_5_d_2, an_unimodular_of_Z_5_d_2.is_unimodular()))

print("{0:s} is unimodular: {1}".format(a_not_unimodular_of_Z_5_d_2, a_not_unimodular_of_Z_5_d_2.is_unimodular()))

print("The value of {0:s} as an element of Z_5_d_2 is {1:s}".format(Z_5(3), base_algebra_as_element_of_the_doubling))

a_full_element_of_the_doubling = Z_5_d_2(Z_5(2), Z_5(3))

print("An element of Z_5_d_2: {0:s}".format(a_full_element_of_the_doubling))

print("Z_5_d_2(Z_5(2), Z_5(3)).dump() = {0}".format(Z_5_d_2(Z_5(2), Z_5(3)).dump()))

print("Z_5_d_2(Z_5(2), Z_5(3)).flatten() = {0}".format(Z_5_d_2(Z_5(2), Z_5(3)).flatten()))

an_element_of_Z_5_d_2_equals_itself = (Z_5_d_2(Z_5(2), Z_5(3)) == Z_5_d_2(Z_5(2), Z_5(3)))

print("Z_5_d_2(Z_5(2), Z_5(3)) == Z_5_d_2(Z_5(2), Z_5(3)) is {0}".format(an_element_of_Z_5_d_2_equals_itself))

two_elements_of_Z_5_d_2_which_are_not_equal = (Z_5_d_2(Z_5(2), Z_5(3)) != Z_5_d_2())

print("Z_5_d_2(Z_5(2), Z_5(3)) != Z_5_d_2() is {0}".format(two_elements_of_Z_5_d_2_which_are_not_equal))

opposite_of_an_element_in_Z_5_d_2 = -Z_5_d_2(Z_5(2), Z_5(3))

print("-Z_5_d_2(Z_5(2), Z_5(3)) = {0:s}".format(opposite_of_an_element_in_Z_5_d_2))

an_addition_in_Z_5_d_2 = Z_5_d_2(Z_5(2), Z_5(3))+Z_5_d_2(Z_5(1), Z_5(4))

print("Z_5_d_2(Z_5(2), Z_5(3))+Z_5_d_2(Z_5(1), Z_5(4)) = {0:s}".format(an_addition_in_Z_5_d_2))

a_substraction_in_Z_5_d_2 = Z_5_d_2(Z_5(2), Z_5(3))-Z_5_d_2(Z_5(1), Z_5(4))

print("Z_5_d_2(Z_5(2), Z_5(3))-Z_5_d_2(Z_5(1), Z_5(4)) = {0:s}".format(a_substraction_in_Z_5_d_2))

class_of_3_in_Z_5 = Z_5(3)

Z_5_d_2_3 = cayley_dickson.cayley_dickson_doubling(Z_5_d_2, class_of_3_in_Z_5)

print("The base ring of Z_5_d_2_3 is {0:s}".format(str(Z_5_d_2_3.base_ring)))
print("The current algebra of Z_5_d_2_3 is {0:s}".format(str(Z_5_d_2_3.current_algebra)))

default_value_in_Z_5_d_2_3 = Z_5_d_2_3()

print("The default value of an element in Z_5_d_2_3 is {0:s}".format(default_value_in_Z_5_d_2_3))

#print(default_value_in_Z_5_d_2_3.flatten_components())
#print(default_value_in_Z_5_d_2_3.__class__.collect_structurals())

class_of_4_in_Z_5 = Z_5(4)

Z_5_d_2_3_4 = cayley_dickson.cayley_dickson_doubling(Z_5_d_2_3, class_of_4_in_Z_5)

print("The base ring of Z_5_d_2_3_4 is {0:s}".format(str(Z_5_d_2_3_4.base_ring)))
print("The current algebra of Z_5_d_2_3_4 is {0:s}".format(str(Z_5_d_2_3_4.current_algebra)))

default_value_in_Z_5_d_2_3_4 = Z_5_d_2_3_4()

print("The default value of an element in Z_5_d_2_3_4 is {0:s}".format(default_value_in_Z_5_d_2_3_4))

an_upcasted_element = Z_5_d_2_3_4.upcast(Z_5(1))

print("Z_5_d_2_3_4.upcast(Z_5(1)) = {0:s}".format(an_upcasted_element))

#print("Z_5_d_2_3_4.upcast_structural = {0:s}".format(Z_5_d_2_3_4.upcast_structural))

a_downcast_element = Z_5_d_2_3_4(Z_5_d_2_3(Z_5_d_2(Z_5(1), Z_5(2)), Z_5_d_2(Z_5(3), Z_5(4))), Z_5_d_2_3(Z_5_d_2(Z_5(1)))).downcast()

print("Z_5_d_2_3_4(Z_5_d_2_3(Z_5_d_2(Z_5(1), Z_5(2)), Z_5_d_2(Z_5(3), Z_5(4))), "+
	"Z_5_d_2_3(Z_5_d_2(Z_5(1)))).downcast() = {0:s}".format(a_downcast_element))

Z_9 = congruence_rings.congruence_ring(9)
Z_9_d_1 = cayley_dickson.cayley_dickson_doubling(Z_9, Z_9(1))
Z_9_d_1_2 = cayley_dickson.cayley_dickson_doubling(Z_9_d_1, Z_9(2))
Z_9_d_1_2_3 = cayley_dickson.cayley_dickson_doubling(Z_9_d_1_2, Z_9(3))

c0 = Z_9_d_1_2(Z_9_d_1(Z_9(1), Z_9(2)), Z_9_d_1(Z_9(3), Z_9(4)))
c1 = Z_9_d_1_2(Z_9_d_1(Z_9(5), Z_9(6)), Z_9_d_1(Z_9(7), Z_9(8)))
an_exterior_product = Z_9(3) @ Z_9_d_1_2_3(c0,c1)

print("Z_9(3) @ Z_9_d_1_2_3(Z_9_d_1_2(Z_9_d_1(Z_9(1), Z_9(2)), Z_9_d_1(Z_9(3), Z_9(4))),"+
	" Z_9_d_1_2(Z_9_d_1(Z_9(5), Z_9(6)), Z_9_d_1(Z_9(7), Z_9(8)))) = "+
	"{0:s}".format(an_exterior_product))

a_conjugation_in_Z_5_d_2 = Z_5_d_2(Z_5(2), Z_5(3)).conjugate()

print("Z_5_d_2(Z_5(2), Z_5(3)).conjugate() = {0:s}".format(a_conjugation_in_Z_5_d_2))

equality_of_the_twice_conjugated = (Z_5_d_2(Z_5(2), Z_5(3)) == a_conjugation_in_Z_5_d_2.conjugate())

print("(Z_5_d_2(Z_5(2), Z_5(3)) == a_conjugation_in_Z_5_d_2.conjugate() is {0}".format(equality_of_the_twice_conjugated))

factor1 = Z_9_d_1_2_3(Z_9_d_1_2(Z_9_d_1(), Z_9_d_1(Z_9(), Z_9(1))), Z_9_d_1_2(Z_9_d_1(Z_9(1))))
factor2 = Z_9_d_1_2_3(Z_9_d_1_2(), Z_9_d_1_2(Z_9_d_1(Z_9(1))))

a_multiplication = factor1*factor2

print("{0:s} * {1:s} = {2:s}".format(factor1, factor2, a_multiplication))

a_cayley_trace = Z_5_d_2(Z_5(2), Z_5(3)).cayley_trace()

print("Z_5_d_2(Z_5(2), Z_5(3)).cayley_trace() = {0:s}".format(a_cayley_trace))

a_cayley_norm = Z_5_d_2(Z_5(2), Z_5(3)).cayley_norm()

print("Z_5_d_2(Z_5(2), Z_5(3)).cayley_norm() = {0:s}".format(a_cayley_norm))

Z_9_d_3 = cayley_dickson.cayley_dickson_doubling(Z_9, Z_9(3))

print("{0:s} is invertible: {1}".format(Z_9_d_3(Z_9(), Z_9(6)),
	Z_9_d_3(Z_9(), Z_9(6)).is_invertible()))

print("{0:s} is invertible: {1}".format(Z_9_d_3(Z_9(2), Z_9(6)),
	Z_9_d_3(Z_9(2), Z_9(6)).is_invertible()))

an_inverse = Z_9_d_3(Z_9(2), Z_9(6)).inverse()

print("The inverse of {0:s} is {1:s}".format(Z_9_d_3(Z_9(2), Z_9(6)), an_inverse))

a_division = Z_9_d_3(Z_9(1), Z_9(5))/Z_9_d_3(Z_9(2), Z_9(6))

print("{0:s} / {1:s} = {2:s}".format(Z_9_d_3(Z_9(1), Z_9(5)), Z_9_d_3(Z_9(2), Z_9(6)), a_division))

an_exponentiation_2 = Z_9_d_3(Z_9(2), Z_9(6))**2

print("Z_9_d_3(Z_9(2), Z_9(6))**2 = {0:s}".format(an_exponentiation_2))

an_exponentiation_4 = Z_9_d_3(Z_9(2), Z_9(6))**4

print("Z_9_d_3(Z_9(2), Z_9(6))**4 = {0:s}".format(an_exponentiation_4))

an_exponentiation_5 = Z_9_d_3(Z_9(2), Z_9(6))**5

print("Z_9_d_3(Z_9(2), Z_9(6))**5 = {0:s}".format(an_exponentiation_5))

Z_9_d_3_3 = cayley_dickson.cayley_dickson_doubling(Z_9_d_3, Z_9(3))

another_exponentiation = Z_9_d_3_3(an_exponentiation_2)**2

print("Z_9_d_3_3(Z_9_d_3(Z_9(2), Z_9(6))**2 = {0:s}".format(another_exponentiation))

a_more_complete_exponentiation = Z_9_d_3_3(Z_9_d_3(Z_9(2), Z_9(6)), Z_9_d_3(Z_9(1), Z_9(5)))**2

print("Z_9_d_3_3(Z_9_d_3(Z_9(2), Z_9(6)), Z_9_d_3(Z_9(1), Z_9(5)))**2 = {0:s}".format(a_more_complete_exponentiation))

yet_another_exponentiation = Z_9_d_3_3(an_exponentiation_2, an_exponentiation_2)**3

print("Z_9_d_3_3(an_exponentiation_2, an_exponentiation_2)**3 = {0:s}".format(yet_another_exponentiation))

Z_9_d_3_3_3 = cayley_dickson.cayley_dickson_doubling(Z_9_d_3_3, Z_9(3))

still_another_exponentiation = Z_9_d_3_3_3(Z_9_d_3_3(an_exponentiation_2))**2

print("Z_9_d_3_3_3(Z_9_d_3_3(an_exponentiation_2))**2 = {0:s}".format(still_another_exponentiation))

a_full_octonion =\
	Z_9_d_3_3_3(Z_9_d_3_3(Z_9_d_3(Z_9(1), Z_9(2)), Z_9_d_3(Z_9(3), Z_9(4))),
	Z_9_d_3_3(Z_9_d_3(Z_9(5), Z_9(6)), Z_9_d_3(Z_9(7), Z_9(8))))

exponentiation_of_the_full = a_full_octonion**3

products_of_the_full = a_full_octonion*(a_full_octonion*a_full_octonion)

putative_equality = (exponentiation_of_the_full == products_of_the_full)

print("a_full_octonion = {0:s}".format(a_full_octonion))
print("a_full_octonion**3 = {0:s}".format(exponentiation_of_the_full))
print("a_full_octonion*(a_full_octonion*a_full_octonion) = {0:s}".format(products_of_the_full))
print("a_full_octonion**3 == a_full_octonion*(a_full_octonion*a_full_octonion) is", putative_equality)

Z_3 = congruence_rings.congruence_ring(3)

try:
	
	class_of_2_in_Z_3 = Z_3(2)
	
	a_meaningless_doubling = cayley_dickson.cayley_dickson_doubling(Z_5, class_of_2_in_Z_3)

except TypeError as err:
	
	print("That's incoherent! {0:s}".format(str(err.args[0])))

try:
	
	a_malformed_element = Z_5_d_2(Z_3(2), Z_5(3))

except TypeError as err:
	
	print("That's also incoherent! {0:s}".format(str(err.args[0])))


# In the end, there can be None

sys.exit(0)



