#!/usr/bin/env python3
"""
Demo_RSA_dev

Invoquer avec: python3 chemin_complet_de_ce_script …

Paramètres positionnels (obligatoires):

exemple:		Chemin complet d'un fichier servant d'exemple.

cryptogramme:	Chemin complet du cryptogramme correspondant à exemple.

decryption:		Chemin complet du message decrypté correspondant à exemple.

Paramètres avec mot-clef (optionels):

reproductible:	Graine des générateurs aléatoires fixée

Ce script lance une démonstration des différents éléments implémentés.
"""


import os
import sys
import argparse
import random


#import numpy


import euclide_etendu

import crypto_config

try:
	
	import coeur_RSA

except Exception as exc:
	
	print(exc)
	
	sys.exit(-1)
	


# Utilitaires

inch = 2.54	# cm

sys.stdout = open(1, 'w', encoding='utf-8', closefd=False)	# Pour sortie en Unicode sans peine

def test_euclide_etendu():
	
	print("Test de l'algorithme d'Euclide étendu\n")
	
	a = 5630
	b = 721
	
	coef_a, coef_b, le_pgcd = euclide_etendu.euclide_etendu(a, b)
	
	print("a = {:0}; b = {:1}; ".format(a, b)+
		"coefficient de a = {:0}; coefficient de b = {:1};".format(coef_a, coef_b)+
		" PGCD(a,b) = {:0}".format(le_pgcd))
	
	print("\n")
	
	print("Vérification: a*coef_a+b*coef_b = ", a*coef_a+b*coef_b)
	
	print("\n")


def test_coeur_RSA():
	
	print("Test du cœur RSA\n")
	
	exposant_publique = 3
	taille_paquet = 6
	
	print("Test 'generateur_clé_prive'\n")
	
	cle_privee = coeur_RSA.generateur_cle_privee(exposant_publique, taille_paquet)
	
	print("Clef privée: ", cle_privee, "\n")
	
	print("Test 'trouve_exposant_prive'\n")
	
	exposant_prive = coeur_RSA.trouve_exposant_prive(cle_privee, exposant_publique)
	
	print("exposant privée: ", exposant_prive, "\n")
	
	print("Test 'generateur_cle_publique'\n")
	
	cle_publique = coeur_RSA.generateur_cle_publique(cle_privee, exposant_publique)
	print("Clef publique: ", cle_publique)
	
	message = 12345667890
	
	print("Nombre à communiquer: ", message)
	
	print("Test 'encode_RSA'\n") #encrypte plutot?
	
	cryptogramme = coeur_RSA.encrypte_RSA(message, cle_publique[1], cle_publique[0])
	print("cryptogramme:", cryptogramme, "\n")
	
	print("Test 'decode_RSA'\n") #id
	
	decryption = coeur_RSA.decrypte_RSA(cryptogramme, exposant_prive, cle_publique[0]) #clef privée plutot
	print("decryption: ", decryption, "\n")
	
	print("Ça marche? ", message == decryption)
	
	print("\n")


def test_tout_RSA(chemin_message, chemin_cryptogramme, chemin_decryption):
	
	taille_paquet = 6
	expo_publique = 3
	cles_privee = coeur_RSA.generateur_cle_privee(expo_publique, taille_paquet)
	cles_publique = coeur_RSA.generateur_cle_publique(cles_privee, expo_publique)
	expo_privee = coeur_RSA.trouve_exposant_prive(cles_privee, expo_publique)
	
# 	with open(chemin_message, "rb") as fp, open(chemin_cryptogramme, "w", newline = None) as cg:
# 		
# 		encryption_fini = False
# 		
# 		while not encryption_fini:
# 			
# 			lu = fp.read(taille_paquet)
# 			
# 			lu_entier = coeur_RSA.encode_RSA(lu)
# 			
# 			cryptogramme = coeur_RSA.encrypte_RSA(lu_entier, cles_publique[1], cles_publique[0])
# 			
# 			decryption = coeur_RSA.decrypte_RSA(cryptogramme, expo_privee, cles_publique[0])
# 			
# 			paquet = coeur_RSA.decode_RSA(decryption)
# 			
# 			print(lu == paquet, lu, paquet)
# 			
# 			if len(lu) != taille_paquet:
# 				
# 				encryption_fini = True
	
	with open(chemin_message, "rb") as fp, open(chemin_cryptogramme, "w", newline = None) as cg:
		
		encryption_fini = False
		
		while not encryption_fini:
			
			lu = fp.read(taille_paquet)
			
			lu_entier = coeur_RSA.encode_RSA(lu)
			
			cryptogramme = coeur_RSA.encrypte_RSA(lu_entier, cles_publique[1], cles_publique[0])
			
			cg.write(str(cryptogramme)+"\n")
			
			if len(lu) != taille_paquet:
				
				encryption_fini = True

		
	with open(chemin_cryptogramme, "r") as cg, open(chemin_decryption, "wb") as dc:
		
		for line in iter(cg.readline, ''):
			
			decryption = coeur_RSA.decrypte_RSA(int(line), expo_privee, cles_publique[0])
			
			paquet = coeur_RSA.decode_RSA(decryption)
			
			dc.write(bytes(paquet))






# 	with open(chemin, "r", newline = None) as fp:
# 		
# 		for ligne in iter(fp.readline, ''):
# 			
# 			print(ligne)
# 			
# 			octet_ligne = bytes(ligne, 'utf-8')
# 			
# # 			
# # 			#print(octet_ligne, "\n")
# # 			hexa_ligne = octet_ligne.hex() 
# # 			#print(hexa_ligne)
# # 			
# # 			dump = ""
# # 			
# # 			for toto in hexa_ligne:
# # 				
# # 				dump += toto
# # 				dump += " "
# 			
# 			octet_par_octet = [bytes([b]) for b in octet_ligne]
# 			
# 			dump = ""
# 			
# 			for toto in octet_par_octet:
# 				
# 				dump += toto.hex()
# 				dump += " " 
# 			
# 			print(dump)


def test_tout_RSA_w_junk(chemin_message, chemin_cryptogramme, chemin_decryption):
	
	taille_paquet = 6
	nb_poubelle = 1
	taille_effective = taille_paquet + nb_poubelle
	expo_publique = 3
	cles_privee = coeur_RSA.generateur_cle_privee(expo_publique, taille_effective)
	cles_publique = coeur_RSA.generateur_cle_publique(cles_privee, expo_publique)
	expo_privee = coeur_RSA.trouve_exposant_prive(cles_privee, expo_publique)
	
	with open(chemin_message, "rb") as fp, open(chemin_cryptogramme, "w", newline = None) as cg:
		
		encryption_fini = False
		
		while not encryption_fini:
			
			lu = fp.read(taille_paquet)
			
			lu_entier = coeur_RSA.encode_RSA_junked(lu)
			
			cryptogramme = coeur_RSA.encrypte_RSA(lu_entier, cles_publique[1], cles_publique[0])
			
			cg.write(str(cryptogramme)+"\n")
			
			if len(lu) != taille_paquet:
				
				encryption_fini = True

		
	with open(chemin_cryptogramme, "r") as cg, open(chemin_decryption, "wb") as dc:
		
		for line in iter(cg.readline, ''):
			
			decryption = coeur_RSA.decrypte_RSA(int(line), expo_privee, cles_publique[0])
			
			paquet = coeur_RSA.decode_RSA_junked(decryption, taille_paquet)
			
			dc.write(bytes(paquet))



def test_tout():
	
	print("\n"+"Ensemble des tests\n")
	
	#test_euclide_etendu()
	test_coeur_RSA()
	#test_tout_RSA(chemin_exemple, chemin_cryptogramme, chemin_decryption)
	#test_tout_RSA_w_junk(chemin_exemple, chemin_cryptogramme, chemin_decryption)


# CLI exploitation

chemin_commun = os.path.dirname(sys.argv[0])

parser = argparse.ArgumentParser(description = "Ce script lance une "+
	"démonstration des différents éléments implémentés.")

parser.add_argument("exemple", help="Chemin complet d'un fichier servant d'exemple.",
	type = str)

parser.add_argument("cryptogramme", help="Chemin complet du cryptogramme correspondant à exemple.",
	type = str)

parser.add_argument("decryption", help="Chemin complet du message decrypté correspondant à exemple.",
	type = str)

parser.add_argument("--reproductible", help="Graine des générateurs aléatoires fixée",
	type = bool, default = False)

args = parser.parse_args()

chemin_exemple = args.exemple

chemin_cryptogramme = args.cryptogramme

chemin_decryption = args.decryption

crypto_config.reproductibilité = args.reproductible


# Schmurf

# Gestion de la reproductibilité

if crypto_config.reproductibilité:# and not générateurs_initialisés:
	
	random.seed(crypto_config.graine_des_générateurs_aléatoires)
	
	#générateurs_initialisés = True

# Tests

test_tout()


# In the end, there can be None

sys.exit(0)
