#!/usr/bin/env python3
"""
euclide_etendu

Utilitaire.
"""


def euclide_etendu(num1 , num2):
	"""
	Calcul du PGCD et de coefficients de Bezout
	par l'algorithme d'Euclide étendu (descente).
	Renvoie un tuple contenant, dans l'ordre,un coefficient de Bezout de num1,
	un coefficient de Bezout de num2, et le PGCD
	"""
	res = [None , None , None]
	
	if num1 > num2 : 
		max = num1
		min = num2
		flag = True
	else:
		max = num2
		min= num1
		flag = False
	
	reste = [max]
	
	while min > 0:
		
		max , min = min , max % min
		reste.append(max)
	
	res[2] = reste[-1]
	
	coef_encore_avant = (1 , 0)
	coef_avant =  (0 , 1)
	coef = (None , None)
	
	if len(reste) >2:
		
		for i in range(0, len(reste)-2):
			
			q = reste[i]//reste[i+1]
			coef =  (coef_encore_avant[0] - coef_avant[0]*q, coef_encore_avant[1] - coef_avant[1]*q)
			coef_encore_avant = coef_avant
			coef_avant = coef
	
	else:
		coef = (0, 1)
		
	
	if flag:
		res[0] = coef[0]
		res[1] = coef[1]
	else:
		res[0] = coef[1]
		res[1] = coef[0]
	
	return tuple(res)















